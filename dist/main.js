(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/admin/admin.module": [
		"./src/app/views/admin/admin.module.ts",
		"views-admin-admin-module"
	],
	"./views/dashboard/dashboard.module": [
		"./src/app/views/dashboard/dashboard.module.ts",
		"views-dashboard-dashboard-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/_nav.ts":
/*!*************************!*\
  !*** ./src/app/_nav.ts ***!
  \*************************/
/*! exports provided: navItemsAdmin, navItemsSoporte */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItemsAdmin", function() { return navItemsAdmin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItemsSoporte", function() { return navItemsSoporte; });
var navItemsAdmin = [
    {
        title: true,
        name: 'Administrador'
    },
    {
        name: 'Inicio',
        url: '/admin',
        icon: 'icon-home',
    },
    {
        name: 'Administradores',
        url: '/usuarios-internos',
        icon: 'icon-user',
        children: [
            {
                name: 'Registro / Consulta',
                url: '/admin/administrador/registrar-internos',
                class: "barnav",
            }
        ]
    },
    {
        name: 'Conductores',
        url: '/usuarios-externos',
        icon: 'icon-user',
        children: [
            {
                name: 'Registro / Consulta',
                url: '/admin/administrador/registrar-externos',
                class: "barnav",
            },
            {
                name: 'Asignar Ruta-Conductor',
                url: '/admin/administrador/registrar-ruta-conductor',
                class: "barnav",
            }
        ]
    },
    {
        name: 'Itinerarios',
        url: '/itinerario',
        icon: 'icon-calendar',
        children: [
            {
                name: 'Registrar Itinerario',
                url: '/admin/administrador/registrar-itinerario',
                class: "barnav",
            },
            {
                name: 'Listar Itinerarios',
                url: '/admin/administrador/listar-itinerarios',
                class: "barnav",
            }
        ]
    },
    {
        name: 'Rutas',
        url: '/rutas',
        icon: 'icon-map',
        children: [
            {
                name: 'Registrar Ruta',
                url: '/admin/administrador/registrar-rutas',
                class: "barnav",
            },
            {
                name: 'Listar Rutas',
                url: '/admin/administrador/listar-rutas',
                class: "barnav",
            }
        ]
    },
    {
        name: 'Reportes',
        url: '/reportes',
        icon: 'icon-graph',
        children: [
            {
                name: 'Reporte De Usuarios',
                url: '/admin/administrador/reporte-usuarios',
                class: "barnav",
            },
            {
                name: 'Reporte De Conductores',
                url: '/admin/administrador/reporte-conductores',
                class: "barnav",
            },
            {
                name: 'Reporte De Itinerarios',
                url: '/admin/administrador/reporte-itinerarios',
                class: "barnav",
            },
            {
                name: 'Reporte De Rutas',
                url: '/admin/administrador/reporte-rutas',
                class: "barnav",
            }
        ]
    },
    {
        name: 'Notificaciones',
        url: '/admin/administrador/notificaciones',
        icon: 'icon-bell'
    },
    {
        name: 'Itinerarios en curso',
        url: '/admin/administrador/itinerarios-en-curso',
        icon: 'icon-hourglass'
    }
];
var navItemsSoporte = [
    {
        title: true,
        name: 'Soporte'
    },
    {
        name: 'Inicio',
        url: '/admin',
    },
    {
        name: 'Conductores',
        url: '/usuarios-externos',
        icon: 'icon-user',
        children: [
            {
                name: 'Registro / Consulta',
                url: '/admin/administrador/registrar-externos',
            },
            {
                name: 'Asignar Ruta-Conductor',
                url: '/admin/administrador/registrar-ruta-conductor',
            }
        ]
    },
    {
        name: 'Itinerarios',
        url: '/itinerario',
        icon: 'icon-calendar',
        children: [
            {
                name: 'Registrar Itinerario',
                url: '/admin/administrador/registrar-itinerario',
            },
            {
                name: 'Listar Itinerarios',
                url: '/admin/administrador/listar-itinerarios',
            }
        ]
    },
    {
        name: 'Rutas',
        url: '/rutas',
        icon: 'icon-map',
        children: [
            {
                name: 'Registrar Ruta',
                url: '/admin/administrador/registrar-rutas',
            },
            {
                name: 'Listar Rutas',
                url: '/admin/administrador/listar-rutas',
            }
        ]
    },
    {
        name: 'Reportes',
        url: '/reportes',
        icon: 'icon-graph',
        children: [
            {
                name: 'Reporte De Usuarios',
                url: '/admin/administrador/reporte-usuarios',
            },
            {
                name: 'Reporte De Conductores',
                url: '/admin/administrador/reporte-conductores',
            },
            {
                name: 'Reporte De Itinerarios',
                url: '/admin/administrador/reporte-itinerarios'
            },
            {
                name: 'Reporte De Rutas',
                url: '/admin/administrador/reporte-rutas',
            }
        ]
    },
    {
        name: 'Notificaciones',
        url: '/admin/administrador/notificaciones',
        icon: 'icon-bell'
    },
    {
        name: 'Itinerarios en curso',
        url: '/admin/administrador/itinerarios-en-curso',
        icon: 'icon-hourglass'
    }
];


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () {
        this.router.events.subscribe(function (evt) {
            if (!(evt instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"])) {
                return;
            }
            window.scrollTo(0, 0);
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            // tslint:disable-next-line
            selector: 'body',
            template: '<router-outlet></router-outlet>'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! angular-6-datatable */ "./node_modules/angular-6-datatable/index.js");
/* harmony import */ var angular_6_datatable__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(angular_6_datatable__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _containers__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./containers */ "./src/app/containers/index.ts");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _coreui_angular__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @coreui/angular */ "./node_modules/@coreui/angular/fesm5/coreui-angular.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "./node_modules/ngx-bootstrap/tabs/fesm5/ngx-bootstrap-tabs.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_23___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_23__);
/* harmony import */ var ngx_loading__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-loading */ "./node_modules/ngx-loading/fesm5/ngx-loading.js");
/* harmony import */ var ng2_ckeditor__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ng2-ckeditor */ "./node_modules/ng2-ckeditor/lib/bundles/ng2-ckeditor.umd.min.js");
/* harmony import */ var ng2_ckeditor__WEBPACK_IMPORTED_MODULE_25___default = /*#__PURE__*/__webpack_require__.n(ng2_ckeditor__WEBPACK_IMPORTED_MODULE_25__);
/* harmony import */ var _views_page_page_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./views/page/page.component */ "./src/app/views/page/page.component.ts");
/* harmony import */ var _views_home_home_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./views/home/home.component */ "./src/app/views/home/home.component.ts");













//import { NgxSpinnerModule } from "ngx-spinner";
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};

// Import containers





var APP_CONTAINERS = [
    _containers__WEBPACK_IMPORTED_MODULE_14__["DefaultLayoutComponent"]
];

// Import routing module

// Import 3rd party components







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_20__["AppRoutingModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_19__["AppAsideModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_19__["AppBreadcrumbModule"].forRoot(),
                _coreui_angular__WEBPACK_IMPORTED_MODULE_19__["AppFooterModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_19__["AppHeaderModule"],
                _coreui_angular__WEBPACK_IMPORTED_MODULE_19__["AppSidebarModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_11__["PerfectScrollbarModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_21__["BsDropdownModule"].forRoot(),
                ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_22__["TabsModule"].forRoot(),
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_23__["ChartsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_5__["HttpModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_7__["ModalModule"].forRoot(),
                ngx_loading__WEBPACK_IMPORTED_MODULE_24__["NgxLoadingModule"].forRoot({}),
                angular_6_datatable__WEBPACK_IMPORTED_MODULE_12__["DataTableModule"],
                ng2_ckeditor__WEBPACK_IMPORTED_MODULE_25__["CKEditorModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_8__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_9__["environment"].firebase),
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_10__["AngularFirestoreModule"],
            ],
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"],
                APP_CONTAINERS,
                _views_error_404_component__WEBPACK_IMPORTED_MODULE_15__["P404Component"],
                _views_error_500_component__WEBPACK_IMPORTED_MODULE_16__["P500Component"],
                _views_login_login_component__WEBPACK_IMPORTED_MODULE_17__["LoginComponent"],
                _views_register_register_component__WEBPACK_IMPORTED_MODULE_18__["RegisterComponent"],
                _views_page_page_component__WEBPACK_IMPORTED_MODULE_26__["PageComponent"],
                _views_home_home_component__WEBPACK_IMPORTED_MODULE_27__["HomeComponent"]
            ],
            providers: [{
                    provide: _angular_common__WEBPACK_IMPORTED_MODULE_3__["LocationStrategy"],
                    useClass: _angular_common__WEBPACK_IMPORTED_MODULE_3__["HashLocationStrategy"]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_13__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: routes, AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _containers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./containers */ "./src/app/containers/index.ts");
/* harmony import */ var _views_error_404_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./views/error/404.component */ "./src/app/views/error/404.component.ts");
/* harmony import */ var _views_error_500_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./views/error/500.component */ "./src/app/views/error/500.component.ts");
/* harmony import */ var _views_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./views/login/login.component */ "./src/app/views/login/login.component.ts");
/* harmony import */ var _views_register_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./views/register/register.component */ "./src/app/views/register/register.component.ts");
/* harmony import */ var _views_page_page_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./views/page/page.component */ "./src/app/views/page/page.component.ts");
/* harmony import */ var _views_home_home_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./views/home/home.component */ "./src/app/views/home/home.component.ts");
/* harmony import */ var _services_admin_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/admin.guard */ "./src/app/services/admin.guard.ts");



// Import Containers







//Guards

var routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '404',
        component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_4__["P404Component"],
        data: {
            title: 'Page 404'
        }
    },
    {
        path: '500',
        component: _views_error_500_component__WEBPACK_IMPORTED_MODULE_5__["P500Component"],
        data: {
            title: 'Page 500'
        }
    },
    {
        path: 'home',
        component: _views_home_home_component__WEBPACK_IMPORTED_MODULE_9__["HomeComponent"],
        data: {
            title: 'Home Page'
        }
    },
    {
        path: 'login',
        component: _views_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
        data: {
            title: 'Login Page'
        }
    },
    {
        path: 'register',
        component: _views_register_register_component__WEBPACK_IMPORTED_MODULE_7__["RegisterComponent"],
        data: {
            title: 'Register Page'
        }
    },
    {
        path: 'page',
        component: _views_page_page_component__WEBPACK_IMPORTED_MODULE_8__["PageComponent"],
        data: {
            title: ''
        }
    },
    {
        path: 'admin',
        component: _containers__WEBPACK_IMPORTED_MODULE_3__["DefaultLayoutComponent"],
        canActivate: [_services_admin_guard__WEBPACK_IMPORTED_MODULE_10__["AdminGuard"]],
        data: {
            title: 'Admin'
        },
        children: [
            {
                path: '',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'administrador',
                loadChildren: './views/admin/admin.module#AdminModule'
            }
        ]
    },
    { path: '**', component: _views_error_404_component__WEBPACK_IMPORTED_MODULE_4__["P404Component"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/containers/default-layout/default-layout.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/containers/default-layout/default-layout.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header\r\n  [navbarBrandRouterLink]=\"['/admin']\"\r\n  [fixed]=\"true\"\r\n  [navbarBrandFull]=\"{src: 'assets/img/logo/logo.png', alt: 'CoreUI Logo', height: '50px'}\"\r\n  [navbarBrandMinimized]=\"{src: 'assets/img/logo/logo_m.png', alt: 'CoreUI Logo', height: '50px'}\"\r\n  [sidebarToggler]=\"'lg'\"\r\n  [asideMenuToggler]=\"'lg'\">\r\n  <ul class=\"nav navbar-nav ml-auto\">\r\n    <li class=\"nav-item dropdown\" dropdown placement=\"bottom right\">\r\n      <a class=\"nav-link\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle (click)=\"false\">\r\n        <img src=\"assets/perfil.png\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\"/>\r\n      </a>\r\n      <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu aria-labelledby=\"simple-dropdown\">\r\n        <a class=\"dropdown-item sans-regular\" href=\"#admin/administrador/perfil\"><i class=\"fa fa-user\"></i> Perfil</a>\r\n         <div class=\"divider\"></div>\r\n        <a class=\"dropdown-item sans-regular\" href=\"#\" (click)=\"cerrarSession()\"><i class=\"fa fa-lock\"></i> Cerrar Session</a>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</app-header>\r\n<div class=\"app-body\" style=\"background: white !important;\">\r\n  <app-sidebar class=\"sidebar-admin\" [fixed]=\"true\" [display]=\"'lg'\">\r\n    <app-sidebar-nav [navItems]=\"navItems\" [perfectScrollbar] [disabled]=\"sidebarMinimized\"></app-sidebar-nav>\r\n    <app-sidebar-minimizer></app-sidebar-minimizer>\r\n  </app-sidebar>\r\n  <!-- Main content -->\r\n  <main class=\"main\">\r\n    <!-- Breadcrumb -->\r\n    <!-- breaking change 'cui-breadcrumb' -->\r\n    <cui-breadcrumb>\r\n      <!-- Breadcrumb Menu-->\r\n   \r\n    </cui-breadcrumb>\r\n    <!-- deprecation warning for 'app-breadcrumb' -->\r\n    <!--<ol class=\"breadcrumb\">-->\r\n      <!--<app-breadcrumb></app-breadcrumb>-->\r\n      <!--&lt;!&ndash; Breadcrumb Menu&ndash;&gt;-->\r\n      <!--<li class=\"breadcrumb-menu d-md-down-none\">-->\r\n        <!--<div class=\"btn-group\" role=\"group\" aria-label=\"Button group with nested dropdown\">-->\r\n          <!--<a class=\"btn\" href=\"#\"><i class=\"icon-speech\"></i></a>-->\r\n          <!--<a class=\"btn\" [routerLink]=\"['/dashboard']\"><i class=\"icon-graph\"></i> &nbsp;Dashboard</a>-->\r\n          <!--<a class=\"btn\" href=\"#\"><i class=\"icon-settings\"></i> &nbsp;Settings</a>-->\r\n        <!--</div>-->\r\n      <!--</li>-->\r\n    <!--</ol>-->\r\n    <div class=\"container-fluid\">\r\n      <router-outlet></router-outlet>\r\n    </div><!-- /.container-fluid -->\r\n  </main>\r\n\r\n</div>\r\n<app-footer>\r\n  <span><a href=\"#/admin\">Titivan</a> &copy; 2019.</span>\r\n  <!--span class=\"ml-auto\"> <a href=\"https://coreui.io/angular\"></a></span-->\r\n</app-footer>\r\n"

/***/ }),

/***/ "./src/app/containers/default-layout/default-layout.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/containers/default-layout/default-layout.component.ts ***!
  \***********************************************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return DefaultLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../_nav */ "./src/app/_nav.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var DefaultLayoutComponent = /** @class */ (function () {
    function DefaultLayoutComponent(_router, _document) {
        var _this = this;
        this._router = _router;
        this.sidebarMinimized = true;
        this.perfil = JSON.parse(localStorage.getItem('usuario'));
        this.changes = new MutationObserver(function (mutations) {
            _this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
        });
        this.element = _document.body;
        this.changes.observe(this.element, {
            attributes: true,
            attributeFilter: ['class']
        });
    }
    DefaultLayoutComponent.prototype.ngOnInit = function () {
        if (this.perfil[0].id_tipo_admin === 1) {
            this.navItems = _nav__WEBPACK_IMPORTED_MODULE_3__["navItemsAdmin"];
        }
        else {
            if (this.perfil[0].id_tipo_admin === 2) {
                this.navItems = _nav__WEBPACK_IMPORTED_MODULE_3__["navItemsSoporte"];
            }
        }
        console.log(this.perfil[0]);
        /*
            if(this.perfil[0].id_perfil_admin===1){
              this.navItems=navItemsAdmin;
            }
        */
    };
    DefaultLayoutComponent.prototype.ngOnDestroy = function () {
        this.changes.disconnect();
    };
    DefaultLayoutComponent.prototype.cerrarSession = function () {
        localStorage.clear();
        this._router.navigate(['/login']);
    };
    DefaultLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./default-layout.component.html */ "./src/app/containers/default-layout/default-layout.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], Object])
    ], DefaultLayoutComponent);
    return DefaultLayoutComponent;
}());



/***/ }),

/***/ "./src/app/containers/default-layout/index.ts":
/*!****************************************************!*\
  !*** ./src/app/containers/default-layout/index.ts ***!
  \****************************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _default_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./default-layout.component */ "./src/app/containers/default-layout/default-layout.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return _default_layout_component__WEBPACK_IMPORTED_MODULE_0__["DefaultLayoutComponent"]; });




/***/ }),

/***/ "./src/app/containers/index.ts":
/*!*************************************!*\
  !*** ./src/app/containers/index.ts ***!
  \*************************************/
/*! exports provided: DefaultLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _default_layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./default-layout */ "./src/app/containers/default-layout/index.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultLayoutComponent", function() { return _default_layout__WEBPACK_IMPORTED_MODULE_0__["DefaultLayoutComponent"]; });




/***/ }),

/***/ "./src/app/services/admin.guard.ts":
/*!*****************************************!*\
  !*** ./src/app/services/admin.guard.ts ***!
  \*****************************************/
/*! exports provided: AdminGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminGuard", function() { return AdminGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./usuario.service */ "./src/app/services/usuario.service.ts");




var AdminGuard = /** @class */ (function () {
    function AdminGuard(_router, _usuarioService) {
        this._router = _router;
        this._usuarioService = _usuarioService;
    }
    AdminGuard.prototype.canActivate = function () {
        var identity = this._usuarioService.getIdentity();
        console.log(identity);
        if (identity && (identity[0].id_tipo_admin === 1 || identity[0].id_tipo_admin === 2)) {
            console.log('login true');
            return true;
        }
        else {
            this._router.navigate(['/login']);
            return false;
        }
    };
    AdminGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"]])
    ], AdminGuard);
    return AdminGuard;
}());



/***/ }),

/***/ "./src/app/services/global.ts":
/*!************************************!*\
  !*** ./src/app/services/global.ts ***!
  \************************************/
/*! exports provided: GLOBAL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GLOBAL", function() { return GLOBAL; });
var GLOBAL = {
    url: 'https://apis-titivan.iteamdevs.com/api/',
    url_image: 'https://apis-titivan.iteamdevs.com/images/conductor/',
};


/***/ }),

/***/ "./src/app/services/usuario.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/usuario.service.ts ***!
  \*********************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var _global__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./global */ "./src/app/services/global.ts");





var UsuarioService = /** @class */ (function () {
    function UsuarioService(_http) {
        this._http = _http;
        this.url = '';
        //public token=localStorage.getItem('token');
        this.token = '';
        this.url = _global__WEBPACK_IMPORTED_MODULE_4__["GLOBAL"].url;
        this.token = 'Bearer ' + localStorage.getItem('token');
        console.log("-- token constructor --");
        console.log(this.token);
    }
    UsuarioService.prototype.login = function (user_to_login) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
        return this._http.post(this.url + 'administrador/login', user_to_login, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.getIdentity = function () {
        var _identity = JSON.parse(localStorage.getItem('usuario'));
        if (_identity !== 'undefined') {
            this.identity = _identity;
        }
        else {
            this.identity = null;
        }
        return this.identity;
    };
    UsuarioService.prototype.registrarUser = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        return this._http.post(this.url + 'administrador/registrar', user, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarAdmins = function (id) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        console.log(this.url);
        return this._http.get(this.url + 'administrador/list/' + id, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarUsers = function (id) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        console.log(this.url);
        return this._http.get(this.url + 'usuario/list/' + id, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarDatosPropios = function () {
        this.token = 'Bearer ' + localStorage.getItem('token');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        console.log(this.url);
        return this._http.get(this.url + 'administrador/list', { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarTiposAdmin = function () {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token
        });
        return this._http.get(this.url + 'administrador/tipos', { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.cambiarEstado = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token
        });
        return this._http.post(this.url + 'administrador/cambiar-estado', user, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.cambiarEstadoUser = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token
        });
        return this._http.post(this.url + 'usuario/cambiar-estado', user, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.editarDatosPropios = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token
        });
        return this._http.post(this.url + 'administrador/editar', user, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.cambiarPassword = function (user) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token
        });
        return this._http.post(this.url + 'administrador/cambiar-password', user, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.recuperarPassword = function (correo) {
        var data = {
            "correo": correo
        };
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
        return this._http.post(this.url + 'administrador/reestablecer-password', data, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.enviarCodigo = function (data) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        });
        return this._http.post(this.url + 'administrador/enviar-codigo', data, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarViajes = function (id) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        return this._http.get(this.url + 'usuario/viajes/' + id, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.listarEstados = function () {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        return this._http.get(this.url + 'viajes/estados', { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService.prototype.enviarNotificaciones = function (mensaje) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': 'application/json',
            'Authorization': this.token,
        });
        return this._http.post(this.url + 'usuario/notificaciones', mensaje, { headers: headers }).map(function (res) { return res.json(); });
    };
    UsuarioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], UsuarioService);
    return UsuarioService;
}());



/***/ }),

/***/ "./src/app/views/error/404.component.html":
/*!************************************************!*\
  !*** ./src/app/views/error/404.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4\">404</h1>\r\n          <h4 class=\"pt-3\">Oops! You're lost.</h4>\r\n          <p class=\"text-muted\">The page you are looking for was not found.</p>\r\n        </div>\r\n        <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/error/404.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/404.component.ts ***!
  \**********************************************/
/*! exports provided: P404Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P404Component", function() { return P404Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P404Component = /** @class */ (function () {
    function P404Component() {
    }
    P404Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./404.component.html */ "./src/app/views/error/404.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], P404Component);
    return P404Component;
}());



/***/ }),

/***/ "./src/app/views/error/500.component.html":
/*!************************************************!*\
  !*** ./src/app/views/error/500.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app flex-row align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row justify-content-center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"clearfix\">\r\n          <h1 class=\"float-left display-3 mr-4\">500</h1>\r\n          <h4 class=\"pt-3\">Houston, we have a problem!</h4>\r\n          <p class=\"text-muted\">The page you are looking for is temporarily unavailable.</p>\r\n        </div>\r\n        <div class=\"input-prepend input-group\">\r\n          <div class=\"input-group-prepend\">\r\n            <span class=\"input-group-text\"><i class=\"fa fa-search\"></i></span>\r\n          </div>\r\n          <input id=\"prependedInput\" class=\"form-control\" size=\"16\" type=\"text\" placeholder=\"What are you looking for?\">\r\n          <span class=\"input-group-append\">\r\n            <button class=\"btn btn-info\" type=\"button\">Search</button>\r\n          </span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/error/500.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/error/500.component.ts ***!
  \**********************************************/
/*! exports provided: P500Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P500Component", function() { return P500Component; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var P500Component = /** @class */ (function () {
    function P500Component() {
    }
    P500Component = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./500.component.html */ "./src/app/views/error/500.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], P500Component);
    return P500Component;
}());



/***/ }),

/***/ "./src/app/views/home/home.component.html":
/*!************************************************!*\
  !*** ./src/app/views/home/home.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid container-banner\">\r\n  <nav class=\"navbar navbar-expand-lg navbar-light row\" >\r\n    <div class=\"container\" style=\"margin-top: 2%;\">\r\n      <div class=\"col-md-4 content-logo\">\r\n          <img class=\"img-login\" src=\"../../../assets/img/logo/logo-titivan-landing.png\" alt=\"\">\r\n      </div>\r\n      <div class=\"col-md-8 offset-md-4 content-menu\" >\r\n        <div class=\"d-flex justify-content-between\">\r\n            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n              <span class=\"navbar-toggler-icon\"></span>\r\n            </button>\r\n            <div class=\"logo-reponsive\"><img src=\"../../../assets/img/logo/logo-titivan-landing.png\" alt=\"\" height=\"55px;\"></div>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n          <ul class=\"navbar-nav mr-auto nav_main\">\r\n            <li class=\"nav-item active\">\r\n              <button class=\"nav-link nav-item--link\" (click)=\"scroll(beneficio)\">Beneficios<span class=\"sr-only\">(current)</span></button>\r\n            </li>\r\n            <li class=\"nav-item active\">\r\n              <button class=\"nav-link nav-item--link\" (click)=\"scroll(servicio)\">Servicios <span class=\"sr-only\">(current)</span></button>\r\n            </li>\r\n            <li class=\"nav-item active\">\r\n              <button class=\"nav-link nav-item--link\" (click)=\"scroll(tiempo)\">Tiempo <span class=\"sr-only\">(current)</span></button>\r\n            </li>\r\n            <li class=\"nav-item active\">\r\n              <button class=\"nav-link nav-item--link\" (click)=\"scroll(ruta)\">Rutas <span class=\"sr-only\">(current)</span></button>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </nav>\r\n  <div class=\"container container-text\">\r\n    <div class=\"row\" style=\"align-items:center\">\r\n      <div class=\"col-md-6\">\r\n        <div class=\"title-banner\">\r\n          VIAJA RÁPIDO, CÓMODO Y SEGURO\r\n        </div>\r\n        <div class=\"sub-title-banner\">\r\n          La mejor opción para viajar y en el menor tiempo posible\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6\" class=\"content-movile\">\r\n          <img class=\"img-app\" src=\"../../../assets/img/landing/app.png\" alt=\"\" class=\"img-movile\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"container-fluid container-shop\">\r\n  <div class=\"container d-flex justify-content-center\">\r\n    <a href=\"#\" class=\"redirect-shop\">Descarga la app</a>\r\n  </div>\r\n  <div class=\"container d-flex justify-content-center content-download\" style=\"margin-top:25px;\">\r\n    <img src=\"../../../assets/img/landing/buttons.png\" alt=\"\" height=\"80px\">\r\n    <img src=\"../../../assets/img/landing/google-play.png\" alt=\"\" height=\"80px\">\r\n  </div>\r\n</div>\r\n<div class=\"container-fluid d-flex justify-content-center\" style=\"position: relative;\">\r\n  <div class=\"container container-description\" #beneficio>\r\n    <div class=\"container-description--title\">Beneficios de viajar con Titivan</div>\r\n    <div class=\"row container-description__items\">\r\n      <div class=\"col-md-4\">\r\n        <div class=\"img-description\"><img src=\"../../../assets/img/landing/escudo.png\" alt=\"\"></div>\r\n        <div class=\"text-description\">Cada van cuenta con un chofer altamente capacitado, seguridad y <span style=\"color: #df5454;\">monitoreo en todo momento</span></div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"img-description\"><img src=\"../../../assets/img/landing/coche-minivan.png\" alt=\"\"></div>\r\n        <div class=\"text-description\">LLega a tu destino sin contratiempos y con paradas intermedias establecidas en la ruta. Siempre ofrecemos <span style=\"color: #df5454;\">la ruta más óptima para ti</span></div>\r\n      </div>\r\n      <div class=\"col-md-4\">\r\n        <div class=\"img-description\"><img src=\"../../../assets/img/landing/asiento.png\" alt=\"\"></div>\r\n        <div class=\"text-description\"><span style=\"color: #df5454;\">Reserva tu asiento a través de la app</span> y disfruta de asientos cómodos, aire acondicionado, wifi y terminales de carga</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"container container-service\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 service-left\">\r\n        <div class=\"service-left--text\" #servicio>Titivan es un servicio de transporte compartido en vans con paradas preestablecidass entre los destinos más concurridos de tu viaje</div>\r\n        <div class=\"row service-left--icon\" style=\"margin-top: 30px;\">\r\n          <div class=\"col-md-3\"><img src=\"../../../assets/img/landing/group.png\" alt=\"\" height=\"70px\"></div>\r\n          <div class=\"col-md-3\"><img src=\"../../../assets/img/landing/group-2.png\" alt=\"\" height=\"70px\"></div>\r\n          <div class=\"col-md-3\"><img src=\"../../../assets/img/landing/group-3.png\" alt=\"\" height=\"70px\"></div>\r\n          <div class=\"col-md-3\"><img src=\"../../../assets/img/landing/group-4.png\" alt=\"\" height=\"70px\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-md-6\" class=\"content-car\"><img src=\"../../../assets/img/landing/group-8.png\" alt=\"\" class=\"img-car\"></div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"container-fluid container-time\" style=\"background: #ffffff;\">\r\n  <div class=\"container\">\r\n    <div class=\"title-valuable\" #tiempo>Sabemos que no hay nada mas valioso que tu tiempo, Con Titivan recupéralo y aprovéchalo al máximo.</div>\r\n    <div style=\"text-align: center;\">\r\n    <img src=\"../../../assets/img/landing/group-9.png\" alt=\"\" class=\"img-people\">\r\n      <div class=\"row justify-content-center\" style=\"margin-top: -45px;\">\r\n        <div class=\"content-time\">\r\n          <img src=\"../../../assets/img/landing/cronografo.png\" alt=\"\">\r\n          <div>Con nuestra aplicación optimizaras tu tiempo al máximo para que disfrutes de todo aquello que quieres</div>\r\n        </div>\r\n        <div><img src=\"../../../assets/img/landing/reloj-de-arena.png\" alt=\"\" style=\"margin-left: -100%;\" class=\"reloj-arena\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"container-fluid container-map\">\r\n  <div class=\"container\" #ruta>\r\n    <div class=\"d-flex header-map\">\r\n      <img src=\"../../../assets/img/landing/titi.png\" alt=\"\">\r\n      <div>Rutas</div>\r\n    </div>\r\n    <div class=\"header-items\">\r\n      <div class=\"row\" style=\"margin-top: 65px;\">\r\n          <div class=\"col-md-4\">Chosica - Lima</div>\r\n          <div class=\"col-md-4\">Chosica - San Isidro</div>\r\n          <div class=\"col-md-4\">Chosica - Miraflores</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"container-fluid container-footer\">\r\n  <div class=\"container\">\r\n    <div>Copyright ©2019  Todos los derechos reservados para Titivan - diseñado por IteamDevs.</div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/home/home.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container-banner {\n  background-image: url('banner.png');\n  background-size: cover; }\n\n.content-logo {\n  position: absolute;\n  top: 0; }\n\n.logo-reponsive {\n  display: none; }\n\n.nav_main {\n  width: 100%;\n  justify-content: space-between; }\n\n.nav-item--link {\n  background: none !important;\n  width: 100%;\n  border: none;\n  border-bottom: 1px solid #ffffff !important;\n  color: #ffffff !important;\n  padding: 15px;\n  transition: 0.5s; }\n\n.nav-item--link:hover {\n    color: #df5454 !important;\n    border-bottom: 1px solid #df5454 !important; }\n\n.container-text {\n  margin-top: 10%;\n  color: #ffffff; }\n\n.container-text div {\n    padding-bottom: 2%; }\n\n.title-banner, .sub-title-banner {\n  color: #ffffff !important; }\n\n.title-banner {\n  font-size: 40px; }\n\n.sub-title-banner {\n  font-size: 20px; }\n\n.container-shop {\n  background: #df5454;\n  padding-bottom: 100px; }\n\n.redirect-shop {\n  padding: 10px 100px;\n  background: #ffffff;\n  color: #de5353 !important;\n  font-size: 18px;\n  text-decoration: none;\n  border-radius: 5px;\n  margin-top: -25px;\n  font-weight: 500; }\n\n.container-description {\n  background: #ffffff;\n  position: absolute;\n  margin-top: -85px;\n  border-radius: 10px;\n  padding-bottom: 80px;\n  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5); }\n\n.container-description--title {\n    text-align: center;\n    font-size: 29px;\n    padding: 65px;\n    font-weight: 600; }\n\n.container-description__items > div {\n    padding-left: 45px;\n    padding-right: 45px;\n    text-align: center; }\n\n.img-description {\n  text-align: center; }\n\n.img-description img {\n    height: 60px;\n    margin: 20px; }\n\n.text-description {\n  font-weight: 500; }\n\n.container-service {\n  margin-top: 450px;\n  margin-bottom: 85px; }\n\n.service-left {\n  padding-left: 85px;\n  padding-right: 55px; }\n\n.service-left--text {\n    font-weight: 600;\n    font-size: 18px;\n    padding-top: 60px; }\n\n.title-valuable {\n  text-align: center;\n  font-size: 23px;\n  padding: 40px 270px; }\n\n.img-people {\n  margin-right: 5%; }\n\n.content-time {\n  background: #df5454;\n  width: 65%;\n  margin-left: 15%;\n  height: 180px;\n  margin-top: 45px;\n  border-radius: 10px;\n  color: #ffffff;\n  font-size: 21px;\n  padding: 18px 60px;\n  margin-bottom: 100px; }\n\n.content-time div {\n    line-height: 1.3em;\n    margin-top: 15px;\n    padding: 0px 20px; }\n\n.container-map {\n  background-image: url('screen-shot-2019-08-01-at-11-23-08-am.png');\n  background-size: cover;\n  padding-top: 30px;\n  height: 300px; }\n\n.header-map {\n  align-items: flex-end; }\n\n.header-map div {\n    font-size: 23px;\n    line-height: 1em;\n    margin-left: 25px; }\n\n.header-items {\n  text-align: center;\n  font-size: 23px; }\n\n.container-footer {\n  background: #4a4a4a;\n  color: #ffffff;\n  padding: 15px;\n  text-align: center;\n  letter-spacing: 2.72px;\n  font-size: 14px;\n  font-weight: 500; }\n\n.img-movile {\n  height: 550px; }\n\n@media (max-width: 1200px) {\n  .title-valuable {\n    padding: 40px 170px; }\n  .content-time {\n    margin-left: 10%;\n    padding-left: 0; }\n  .img-people {\n    margin-right: 20%; }\n  .service-left {\n    padding-left: 55px; } }\n\n@media (max-width: 991px) {\n  .service-left {\n    padding-left: 0px;\n    padding-right: 40px; }\n    .service-left--text {\n      padding-top: 20px; }\n  .container-description__items > div {\n    padding-left: 10px;\n    padding-right: 10px; }\n  .img-car {\n    height: 275px; }\n  .title-valuable {\n    padding: 40px 65px; }\n  .img-people {\n    margin-right: 0px; }\n  .content-time {\n    margin-left: 0;\n    padding: 0;\n    width: 80%;\n    padding-top: 25px !important; }\n  .reloj-arena {\n    display: none; }\n  .img-movile {\n    height: 425px; }\n  .content-logo {\n    display: none; }\n  .content-menu {\n    margin-left: 0px;\n    flex: 100%;\n    max-width: 100%; }\n  .logo-reponsive {\n    display: block; }\n    .logo-reponsive img {\n      height: 100px; }\n  .navbar.navbar-expand-lg {\n    padding-top: 0px; }\n    .navbar.navbar-expand-lg div {\n      margin-top: 0px !important; } }\n\n@media (max-width: 850px) {\n  .img-movile {\n    height: 400px; } }\n\n@media (max-width: 767px) {\n  .content-logo img {\n    height: 120px; }\n  .content-movile {\n    text-align: center;\n    width: 100%; }\n  .container-description {\n    padding-bottom: 10px; }\n  .container-description--title {\n    padding: 20px; }\n  .container-description__items > div {\n    display: flex;\n    flex-direction: column-reverse; }\n  .text-description {\n    padding-left: 50px;\n    padding-right: 50px; }\n  .service-left {\n    padding-right: 0px; }\n    .service-left--text {\n      padding-top: 40px;\n      text-align: center; }\n    .service-left--icon {\n      text-align: center; }\n      .service-left--icon div {\n        width: 25%; }\n  .content-car {\n    text-align: center;\n    margin-top: 30px;\n    width: 100%; }\n  .content-time {\n    width: 100%;\n    margin-bottom: 50px; }\n  .content-time div {\n    line-height: 1.3em;\n    margin-top: 0px;\n    padding: 0px 10px; }\n  .title-valuable {\n    padding: 40px 0px; }\n  .container-time {\n    padding-bottom: 30px; }\n  .header-map {\n    justify-content: center; }\n  .title-banner {\n    margin-top: -40px; } }\n\n@media (max-width: 575px) {\n  .container-description {\n    width: 90%; }\n  .content-time div {\n    font-size: 20px; } }\n\n@media (max-width: 545px) {\n  .service-left--text {\n    padding-top: 55px; } }\n\n@media (max-width: 528px) {\n  .service-left--text {\n    padding-top: 120px; } }\n\n@media (max-width: 455px) {\n  .service-left--text {\n    padding-top: 150px; }\n  .img-people {\n    height: 120px; }\n  .text-description {\n    padding-left: 30px;\n    padding-right: 30px; }\n  .content-time {\n    height: auto;\n    padding-bottom: 20px; }\n  .redirect-shop {\n    padding: 10px 65px; }\n  .title-banner {\n    margin-top: 5px; } }\n\n@media (max-width: 375px) {\n  .service-left--text {\n    padding-top: 175px; }\n  .img-car {\n    height: 220px; }\n  .container-service {\n    margin-bottom: 35px; }\n  .service-left--icon {\n    justify-content: center; }\n    .service-left--icon div {\n      padding: 0px;\n      width: 23%; } }\n\n@media (max-width: 350px) {\n  .img-people {\n    height: 95px; }\n  .img-movile {\n    height: 360px; }\n  .content-download img {\n    height: 70px; } }\n\n@media (max-width: 325px) {\n  .service-left--text {\n    padding-top: 225px; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9DOlxcVXNlcnNcXFVzdWFyaW9cXERlc2t0b3BcXFByb3llY3Rvcy1UZWFtLVNKQ1xcVGl0aXZhblxcYWRtaW4tdGl0aXZhbi1mcm9udC9zcmNcXGFwcFxcdmlld3NcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQ0FBK0Q7RUFDL0Qsc0JBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksa0JBQWtCO0VBQ2xCLE1BQUssRUFBQTs7QUFHVDtFQUNJLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxXQUFVO0VBQ1YsOEJBQTZCLEVBQUE7O0FBSTdCO0VBQ0ksMkJBQTJCO0VBQzNCLFdBQVc7RUFDWCxZQUFhO0VBQ2IsMkNBQTJDO0VBQzNDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7O0FBUG5CO0lBVU8seUJBQXlCO0lBQ3pCLDJDQUEyQyxFQUFBOztBQUt2RDtFQUNJLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBRmxCO0lBS1Esa0JBQWtCLEVBQUE7O0FBSTFCO0VBQ0kseUJBQXdCLEVBQUE7O0FBRzVCO0VBQ0ksZUFBZSxFQUFBOztBQUduQjtFQUNJLGVBQWUsRUFBQTs7QUFHbkI7RUFDSSxtQkFBbUI7RUFDbkIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsZUFBZTtFQUNmLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsMENBQTBDLEVBQUE7O0FBRTFDO0lBQ0ksa0JBQWlCO0lBQ2pCLGVBQWU7SUFDZixhQUFhO0lBQ2IsZ0JBQWdCLEVBQUE7O0FBR3BCO0lBQ0ksa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixrQkFBa0IsRUFBQTs7QUFJMUI7RUFDSSxrQkFBa0IsRUFBQTs7QUFEdEI7SUFJUSxZQUFZO0lBQ1osWUFBWSxFQUFBOztBQUlwQjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGlCQUFpQjtFQUNqQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxrQkFBa0I7RUFDbEIsbUJBQW1CLEVBQUE7O0FBRW5CO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWU7SUFDZixpQkFBaUIsRUFBQTs7QUFJekI7RUFDSSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLG1CQUFtQixFQUFBOztBQUd2QjtFQUNJLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLG9CQUFvQixFQUFBOztBQVZ4QjtJQWFRLGtCQUFrQjtJQUFFLGdCQUFnQjtJQUFFLGlCQUFpQixFQUFBOztBQUkvRDtFQUNJLGtFQUE4RjtFQUM5RixzQkFBcUI7RUFDckIsaUJBQWlCO0VBQ2pCLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxxQkFBcUIsRUFBQTs7QUFEekI7SUFJUSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGlCQUFpQixFQUFBOztBQUl6QjtFQUNJLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2YsZ0JBQWdCLEVBQUE7O0FBR3BCO0VBQ0ksYUFBYSxFQUFBOztBQUdqQjtFQUNJO0lBQ0ksbUJBQW1CLEVBQUE7RUFHdkI7SUFDSSxnQkFBZ0I7SUFDaEIsZUFBZSxFQUFBO0VBR25CO0lBQ0ksaUJBQWlCLEVBQUE7RUFHckI7SUFDSSxrQkFBa0IsRUFBQSxFQUVyQjs7QUFHTDtFQUNJO0lBQ0ksaUJBQWlCO0lBQ2pCLG1CQUFtQixFQUFBO0lBRW5CO01BQ0ksaUJBQWlCLEVBQUE7RUFJekI7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CLEVBQUE7RUFHdkI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxrQkFBa0IsRUFBQTtFQUd0QjtJQUNJLGlCQUFpQixFQUFBO0VBR3JCO0lBQ0ksY0FBYztJQUNkLFVBQVU7SUFDVixVQUFVO0lBQ1YsNEJBQTRCLEVBQUE7RUFHaEM7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLGVBQWUsRUFBQTtFQUduQjtJQUNJLGNBQWMsRUFBQTtJQURsQjtNQUlRLGFBQWEsRUFBQTtFQUlyQjtJQUNJLGdCQUFnQixFQUFBO0lBRHBCO01BSVEsMEJBQTBCLEVBQUEsRUFDN0I7O0FBSVQ7RUFDSTtJQUNJLGFBQWEsRUFBQSxFQUNoQjs7QUFHTDtFQUNJO0lBRVEsYUFBYSxFQUFBO0VBSXJCO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVcsRUFBQTtFQUdmO0lBQ0ksb0JBQW9CLEVBQUE7RUFHeEI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxhQUFhO0lBQ2IsOEJBQThCLEVBQUE7RUFHbEM7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CLEVBQUE7RUFHdkI7SUFDSSxrQkFBa0IsRUFBQTtJQUVsQjtNQUNJLGlCQUFpQjtNQUNqQixrQkFBa0IsRUFBQTtJQUV0QjtNQUNJLGtCQUFrQixFQUFBO01BRHJCO1FBSU8sVUFBVSxFQUFBO0VBS3RCO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixXQUFXLEVBQUE7RUFHZjtJQUNJLFdBQVc7SUFDWCxtQkFBbUIsRUFBQTtFQUd2QjtJQUNJLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsaUJBQWlCLEVBQUE7RUFHckI7SUFDSSxpQkFBaUIsRUFBQTtFQUdyQjtJQUNJLG9CQUFvQixFQUFBO0VBR3hCO0lBQ0ksdUJBQXVCLEVBQUE7RUFHM0I7SUFDSSxpQkFBaUIsRUFBQSxFQUNwQjs7QUFHTDtFQUNJO0lBQ0ksVUFBVSxFQUFBO0VBR2Q7SUFDSSxlQUFlLEVBQUEsRUFDbEI7O0FBR0w7RUFDSTtJQUNJLGlCQUFpQixFQUFBLEVBQ3BCOztBQUdMO0VBQ0k7SUFDSSxrQkFBa0IsRUFBQSxFQUNyQjs7QUFHTDtFQUNJO0lBQ0ksa0JBQWtCLEVBQUE7RUFHdEI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CLEVBQUE7RUFHdkI7SUFDSSxZQUFZO0lBQ1osb0JBQW9CLEVBQUE7RUFHeEI7SUFDSSxrQkFBa0IsRUFBQTtFQUd0QjtJQUNJLGVBQWUsRUFBQSxFQUNsQjs7QUFHTDtFQUNJO0lBQ0ksa0JBQWtCLEVBQUE7RUFHdEI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxtQkFBbUIsRUFBQTtFQUd2QjtJQUNJLHVCQUF1QixFQUFBO0lBRDNCO01BSVEsWUFBWTtNQUNaLFVBQVUsRUFBQSxFQUNiOztBQUlUO0VBQ0k7SUFDSSxZQUFZLEVBQUE7RUFHaEI7SUFDSSxhQUFhLEVBQUE7RUFHakI7SUFDSSxZQUFZLEVBQUEsRUFDZjs7QUFHTDtFQUNJO0lBQ0ksa0JBQWtCLEVBQUEsRUFDckIiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyLWJhbm5lcntcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvbGFuZGluZy9iYW5uZXIucG5nXCIpO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOmNvdmVyO1xyXG59XHJcblxyXG4uY29udGVudC1sb2dve1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOjA7XHJcbn1cclxuXHJcbi5sb2dvLXJlcG9uc2l2ZXtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi5uYXZfbWFpbntcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYmV0d2VlbjsgICBcclxufVxyXG5cclxuLm5hdi1pdGVte1xyXG4gICAgJi0tbGlua3tcclxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgYm9yZGVyOiBub25lIDtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2ZmZmZmZiAhaW1wb3J0YW50O1xyXG4gICAgICAgIGNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICB0cmFuc2l0aW9uOiAwLjVzO1xyXG5cclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICBjb2xvcjogI2RmNTQ1NCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RmNTQ1NCAhaW1wb3J0YW50O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmNvbnRhaW5lci10ZXh0e1xyXG4gICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcblxyXG4gICAgZGl2e1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyJTtcclxuICAgIH1cclxufVxyXG5cclxuLnRpdGxlLWJhbm5lciAsIC5zdWItdGl0bGUtYmFubmVye1xyXG4gICAgY29sb3I6I2ZmZmZmZiAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUtYmFubmVye1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcblxyXG4uc3ViLXRpdGxlLWJhbm5lcntcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1zaG9we1xyXG4gICAgYmFja2dyb3VuZDogI2RmNTQ1NDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMDBweDtcclxufVxyXG5cclxuLnJlZGlyZWN0LXNob3B7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDEwMHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGNvbG9yOiAjZGU1MzUzICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuXHJcbi5jb250YWluZXItZGVzY3JpcHRpb257XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbWFyZ2luLXRvcDogLTg1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDgwcHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDJweCA0cHggMCByZ2JhKDAsIDAsIDAsIDAuNSk7XHJcblxyXG4gICAgJi0tdGl0bGV7XHJcbiAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAyOXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDY1cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIH1cclxuXHJcbiAgICAmX19pdGVtcyA+IGRpdntcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDQ1cHg7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogNDVweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5pbWctZGVzY3JpcHRpb257XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW1ne1xyXG4gICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi50ZXh0LWRlc2NyaXB0aW9ue1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5cclxuLmNvbnRhaW5lci1zZXJ2aWNle1xyXG4gICAgbWFyZ2luLXRvcDogNDUwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA4NXB4O1xyXG59XHJcblxyXG4uc2VydmljZS1sZWZ0e1xyXG4gICAgcGFkZGluZy1sZWZ0OiA4NXB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogNTVweDtcclxuXHJcbiAgICAmLS10ZXh0e1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA2MHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4udGl0bGUtdmFsdWFibGV7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICBwYWRkaW5nOiA0MHB4IDI3MHB4O1xyXG59XHJcblxyXG4uaW1nLXBlb3BsZXtcclxuICAgIG1hcmdpbi1yaWdodDogNSU7XHJcbn1cclxuXHJcbi5jb250ZW50LXRpbWV7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZGY1NDU0O1xyXG4gICAgd2lkdGg6IDY1JTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNSU7XHJcbiAgICBoZWlnaHQ6IDE4MHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNDVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtc2l6ZTogMjFweDtcclxuICAgIHBhZGRpbmc6IDE4cHggNjBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwMHB4O1xyXG5cclxuICAgIGRpdntcclxuICAgICAgICBsaW5lLWhlaWdodDogMS4zZW07IG1hcmdpbi10b3A6IDE1cHg7IHBhZGRpbmc6IDBweCAyMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uY29udGFpbmVyLW1hcHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvbGFuZGluZy9zY3JlZW4tc2hvdC0yMDE5LTA4LTAxLWF0LTExLTIzLTA4LWFtLnBuZ1wiKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTpjb3ZlcjtcclxuICAgIHBhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxufVxyXG5cclxuLmhlYWRlci1tYXB7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcblxyXG4gICAgZGl2e1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjNweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMWVtO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4uaGVhZGVyLWl0ZW1ze1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG59XHJcblxyXG4uY29udGFpbmVyLWZvb3RlcntcclxuICAgIGJhY2tncm91bmQ6ICM0YTRhNGE7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMi43MnB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5cclxuLmltZy1tb3ZpbGV7XHJcbiAgICBoZWlnaHQ6IDU1MHB4O1xyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMTIwMHB4KXtcclxuICAgIC50aXRsZS12YWx1YWJsZXtcclxuICAgICAgICBwYWRkaW5nOiA0MHB4IDE3MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250ZW50LXRpbWV7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmltZy1wZW9wbGV7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAyMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLnNlcnZpY2UtbGVmdHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDU1cHg7XHJcblxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogOTkxcHgpe1xyXG4gICAgLnNlcnZpY2UtbGVmdHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA0MHB4O1xyXG5cclxuICAgICAgICAmLS10ZXh0e1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhaW5lci1kZXNjcmlwdGlvbl9faXRlbXMgPiBkaXYge1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWctY2Fye1xyXG4gICAgICAgIGhlaWdodDogMjc1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpdGxlLXZhbHVhYmxle1xyXG4gICAgICAgIHBhZGRpbmc6IDQwcHggNjVweDtcclxuICAgIH1cclxuXHJcbiAgICAuaW1nLXBlb3BsZXtcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGVudC10aW1le1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMjVweCAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWxvai1hcmVuYXtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWctbW92aWxle1xyXG4gICAgICAgIGhlaWdodDogNDI1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRlbnQtbG9nb3tcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250ZW50LW1lbnV7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgICBmbGV4OiAxMDAlO1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICAubG9nby1yZXBvbnNpdmV7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcblxyXG4gICAgICAgIGltZ3tcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci5uYXZiYXItZXhwYW5kLWxne1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcblxyXG4gICAgICAgIGRpdntcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogODUwcHgpe1xyXG4gICAgLmltZy1tb3ZpbGV7XHJcbiAgICAgICAgaGVpZ2h0OiA0MDBweDtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KXtcclxuICAgIC5jb250ZW50LWxvZ297XHJcbiAgICAgICAgaW1ne1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuY29udGVudC1tb3ZpbGV7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250YWluZXItZGVzY3JpcHRpb257XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhaW5lci1kZXNjcmlwdGlvbi0tdGl0bGV7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGFpbmVyLWRlc2NyaXB0aW9uX19pdGVtcyA+IGRpdiB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uLXJldmVyc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLnRleHQtZGVzY3JpcHRpb257XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDUwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnNlcnZpY2UtbGVmdHtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHg7XHJcblxyXG4gICAgICAgICYtLXRleHR7XHJcbiAgICAgICAgICAgIHBhZGRpbmctdG9wOiA0MHB4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYtLWljb257ICAgXHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAgICAgICAgIGRpdntcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAyNSU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRlbnQtY2Fye1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250ZW50LXRpbWV7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGVudC10aW1lIGRpdntcclxuICAgICAgICBsaW5lLWhlaWdodDogMS4zZW07XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC50aXRsZS12YWx1YWJsZSB7XHJcbiAgICAgICAgcGFkZGluZzogNDBweCAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhaW5lci10aW1le1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5oZWFkZXItbWFwe1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC50aXRsZS1iYW5uZXJ7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogLTQwcHg7XHJcbiAgICB9ICBcclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDU3NXB4KXtcclxuICAgIC5jb250YWluZXItZGVzY3JpcHRpb257XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuXHJcbiAgICAuY29udGVudC10aW1lIGRpdntcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiA1NDVweCl7XHJcbiAgICAuc2VydmljZS1sZWZ0LS10ZXh0e1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiA1NXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogNTI4cHgpe1xyXG4gICAgLnNlcnZpY2UtbGVmdC0tdGV4dHtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTIwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiA0NTVweCl7XHJcbiAgICAuc2VydmljZS1sZWZ0LS10ZXh0e1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxNTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuaW1nLXBlb3BsZXtcclxuICAgICAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC50ZXh0LWRlc2NyaXB0aW9ue1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMzBweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250ZW50LXRpbWV7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5yZWRpcmVjdC1zaG9we1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHggNjVweDtcclxuICAgIH1cclxuXHJcbiAgICAudGl0bGUtYmFubmVye1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDM3NXB4KXtcclxuICAgIC5zZXJ2aWNlLWxlZnQtLXRleHR7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDE3NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWctY2Fye1xyXG4gICAgICAgIGhlaWdodDogMjIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhaW5lci1zZXJ2aWNle1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLnNlcnZpY2UtbGVmdC0taWNvbntcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgZGl2e1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiAyMyU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMzUwcHgpe1xyXG4gICAgLmltZy1wZW9wbGV7XHJcbiAgICAgICAgaGVpZ2h0OiA5NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5pbWctbW92aWxle1xyXG4gICAgICAgIGhlaWdodDogMzYwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRlbnQtZG93bmxvYWQgaW1ne1xyXG4gICAgICAgIGhlaWdodDogNzBweDtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDMyNXB4KXtcclxuICAgIC5zZXJ2aWNlLWxlZnQtLXRleHR7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDIyNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/views/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.scroll = function (el) {
        el.scrollIntoView({ behavior: 'smooth' });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/views/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/views/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/views/login/login.component.html":
/*!**************************************************!*\
  !*** ./src/app/views/login/login.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-body\">\r\n  <main class=\"main d-flex align-items-center\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 mx-auto\">\r\n          <div class=\"card-group\">\r\n            <div class=\"card p-4\">\r\n              <div class=\"card-body\">\r\n                <form>\r\n                  <h1>INICIAR SESIÓN</h1>\r\n                  <p class=\"text-muted\">Inicia sessión con tu cuenta</p>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                    </div>\r\n                    <input (focus)=\"removeAlertLogin()\" type=\"text\" class=\"form-control\" placeholder=\"Username\"\r\n                      autocomplete=\"username\" required name=\"correo\" #email=\"ngModel\" [(ngModel)]=\"logeo.correo\">\r\n                  </div>\r\n                  <div class=\"input-group mb-4\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                    </div>\r\n                    <input type=\"password\" class=\"form-control\" placeholder=\"Password\" autocomplete=\"current-password\"\r\n                      required name=\"password\" #contrasena=\"ngModel\" [(ngModel)]=\"logeo.password\">\r\n                  </div>\r\n                  <div class=\"input-group mb-4\">\r\n                    <span *ngIf=\"showAlertLogin\" style=\"width:100%;\"\r\n                      class=\"alert alert-danger\"><strong>{{message}}</strong></span>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"col-12\">\r\n                      <button type=\"button\" id=\"btn-login\" class=\"btn btn-square btn-block btn-danger\" (click)=\"login()\">Iniciar sessión</button>\r\n                    </div>\r\n                    <div class=\"col-12\" id=\"login-recuperar\">\r\n                      <button type=\"button\" id=\"text-login\" class=\"btn btn-link px-0\" (click)=\"recuperarPassword()\">Recuperar\r\n                        contraseña</button> <p *ngIf=\"!recuperar\">Validando datos...</p>\r\n                    </div>\r\n                    \r\n                  </div>\r\n                </form>\r\n              </div>\r\n            </div>\r\n            <div class=\"card text-white bg-primary py-5 d-md-down-none logo-login\" style=\"width:50%\">\r\n              <div class=\"card-body text-center\">\r\n                <div>\r\n                  <img class=\"img-login\" src=\"../../../assets/img/logo/logo_titivan.png\" alt=\"\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n\r\n<div bsModal #dangerModal=\"bs-modal\" data-backdrop=\"static\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-danger\" role=\"document\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">Recuperar Contraseña</h4>\r\n        <button type=\"button\" class=\"close\" (click)=\"dangerModal.hide()\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col-sm-12\">\r\n            <div class=\"form-group\">\r\n              <h1>Código de Verificación</h1>\r\n              <p>Te enviamos un código de verificación al {{logeo.correo}}</p>\r\n              <label>Código</label>\r\n              <input class=\"form-control col-2\" [(ngModel)]=\"reestablecerPassword.codigo\" type=\"text\" placeholder=\"------\" autofocus=\"autofocus\">\r\n            </div>\r\n            \r\n\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-key\"></i></span>\r\n              </div>\r\n              <input [type]=\"passwordtype_new\" [(ngModel)]=\"reestablecerPassword.password\" class=\"form-control\" placeholder=\"Nuevo Password\" required>\r\n              <span class=\"input-group-text\"><i [class]=\"__icon_new\" (click)=\"togglePass(2)\"></i></span>\r\n            </div>\r\n            <div class=\"input-group mb-3\">\r\n              <div class=\"input-group-prepend\">\r\n                <span class=\"input-group-text\"><i class=\"icon-key\"></i></span>\r\n              </div>\r\n              <input [type]=\"passwordtype_conf\" [(ngModel)]=\"passConfirmar\" class=\"form-control\" placeholder=\"Confirmar Password\"\r\n                required>\r\n              <span class=\"input-group-text\"><i [class]=\"__icon_conf\" (click)=\"togglePass(3)\"></i></span>\r\n            </div>\r\n\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"dangerModal.hide()\">Cancelar</button>\r\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"validarCodigo()\">Confirmar</button>\r\n      </div>\r\n    </div><!-- /.modal-content -->\r\n  </div><!-- /.modal-dialog -->\r\n</div><!-- /.modal -->\r\n"

/***/ }),

/***/ "./src/app/views/login/login.component.scss":
/*!**************************************************!*\
  !*** ./src/app/views/login/login.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".app-body {\n  background: #cbcbc9b6 !important; }\n\n.logo-login {\n  background: #F44336 !important;\n  border-color: #F44336 !important; }\n\n.img-login {\n  width: 100%; }\n\n#text-login {\n  color: #F44336; }\n\n#login-recuperar {\n  margin-top: 15%;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbG9naW4vQzpcXFVzZXJzXFxVc3VhcmlvXFxEZXNrdG9wXFxQcm95ZWN0b3MtVGVhbS1TSkNcXFRpdGl2YW5cXGFkbWluLXRpdGl2YW4tZnJvbnQvc3JjXFxhcHBcXHZpZXdzXFxsb2dpblxcbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSxnQ0FBK0IsRUFBQTs7QUFFbkM7RUFDSSw4QkFBNkI7RUFDN0IsZ0NBQStCLEVBQUE7O0FBRW5DO0VBQ0ksV0FBVyxFQUFBOztBQUdmO0VBQ0ksY0FBYyxFQUFBOztBQUVsQjtFQUNJLGVBQWU7RUFDZixrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5hcHAtYm9keXtcclxuICAgIGJhY2tncm91bmQ6ICNjYmNiYzliNiFpbXBvcnRhbnQ7XHJcbn1cclxuLmxvZ28tbG9naW57XHJcbiAgICBiYWNrZ3JvdW5kOiAjRjQ0MzM2IWltcG9ydGFudDtcclxuICAgIGJvcmRlci1jb2xvcjogI0Y0NDMzNiFpbXBvcnRhbnQ7XHJcbn1cclxuLmltZy1sb2dpbntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4jdGV4dC1sb2dpbntcclxuICAgIGNvbG9yOiAjRjQ0MzM2O1xyXG59XHJcbiNsb2dpbi1yZWN1cGVyYXJ7XHJcbiAgICBtYXJnaW4tdG9wOiAxNSU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/login/login.component.ts":
/*!************************************************!*\
  !*** ./src/app/views/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_usuario_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/usuario.service */ "./src/app/services/usuario.service.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/esm5/ngx-bootstrap.js");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(config, modalService, _router, _usuarioService) {
        this.modalService = modalService;
        this._router = _router;
        this._usuarioService = _usuarioService;
        this.loading = false;
        this.logeo = {
            correo: "",
            password: ""
        };
        this.passConfirmar = "";
        this.reestablecerPassword = {
            correo: "",
            codigo: "",
            password: ""
        };
        this.recuperar = true;
        this.passwordtype = "password";
        this.passwordshow = false;
        this.__icon = "icon-lock";
        this.passwordtype_new = "password";
        this.passwordshow_new = false;
        this.__icon_new = "icon-lock";
        this.passwordtype_conf = "password";
        this.passwordshow_conf = false;
        this.__icon_conf = "icon-lock";
        //config.backdrop = 'static';
        //config.keyboard = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        localStorage.clear();
        this.showAlertLogin = false;
        this.message = "";
    };
    LoginComponent.prototype.login = function () {
        //this._router.navigate(['/home']);
        var _this = this;
        this._usuarioService.login(this.logeo).subscribe(function (data) {
            if (data.code === 200) {
                console.log(data.token);
                localStorage.setItem('usuario', JSON.stringify(data.usuario));
                localStorage.setItem('token', data.token);
                _this._router.navigate(['/admin']);
            }
            else {
                _this.message = data.message;
                _this.showAlertLogin = true;
                _this.logeo.password = "";
                _this.logeo.correo = "";
            }
        }, function (error) {
            console.log(error);
        });
    };
    LoginComponent.prototype.removeAlertLogin = function () {
        this.showAlertLogin = false;
    };
    LoginComponent.prototype.recuperarPassword = function () {
        var _this = this;
        if (this.logeo.correo.length != 0) {
            this._usuarioService.recuperarPassword(this.logeo.correo).subscribe(function (data) {
                if (data.code === 200) {
                    _this.logeo.password = "";
                    _this.reestablecerPassword.correo = _this.logeo.correo;
                    _this.recuperar = true;
                    _this.dangerModal.show();
                    _this.dangerModal.config.backdrop = 'static';
                }
                else {
                    alert(data.message);
                }
            }, function (error) {
                console.log(error);
            });
        }
        else {
            alert("Debe ingresar un correo");
        }
    };
    LoginComponent.prototype.validarCodigo = function () {
        var _this = this;
        if (this.passConfirmar == this.reestablecerPassword.password) {
            this._usuarioService.enviarCodigo(this.reestablecerPassword).subscribe(function (data) {
                alert(data.message);
                if (data.code === 200) {
                    _this.dangerModal.hide();
                }
            }, function (error) {
                console.log(error);
            });
        }
        else {
            alert("Las contraseñas no coinciden");
        }
        this.limpiar();
    };
    LoginComponent.prototype.togglePass = function (num) {
        console.log("ver / ocultar");
        switch (num) {
            case 1:
                if (this.passwordshow) {
                    this.passwordtype = "password";
                    this.passwordshow = false;
                    this.__icon = "icon-lock";
                }
                else {
                    this.passwordtype = "text";
                    this.passwordshow = true;
                    this.__icon = "icon-lock-open";
                }
                ;
                break;
            case 2:
                if (this.passwordshow_new) {
                    this.passwordtype_new = "password";
                    this.passwordshow_new = false;
                    this.__icon_new = "icon-lock";
                }
                else {
                    this.passwordtype_new = "text";
                    this.passwordshow_new = true;
                    this.__icon_new = "icon-lock-open";
                }
                break;
            case 3:
                if (this.passwordshow_conf) {
                    this.passwordtype_conf = "password";
                    this.passwordshow_conf = false;
                    this.__icon_conf = "icon-lock";
                }
                else {
                    this.passwordtype_conf = "text";
                    this.passwordshow_conf = true;
                    this.__icon_conf = "icon-lock-open";
                }
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["ModalDirective"];
        }
    };
    LoginComponent.prototype.limpiar = function () {
        this.reestablecerPassword.codigo = "";
        this.reestablecerPassword.password = "";
        this.passConfirmar = "";
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dangerModal'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", ngx_bootstrap__WEBPACK_IMPORTED_MODULE_5__["ModalDirective"])
    ], LoginComponent.prototype, "dangerModal", void 0);
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/views/login/login.component.html"),
            providers: [_services_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModalConfig"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"]],
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/views/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModalConfig"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/views/page/page.component.html":
/*!************************************************!*\
  !*** ./src/app/views/page/page.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  page works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/views/page/page.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/page/page.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2UvcGFnZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/page/page.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/page/page.component.ts ***!
  \**********************************************/
/*! exports provided: PageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageComponent", function() { return PageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageComponent = /** @class */ (function () {
    function PageComponent() {
    }
    PageComponent.prototype.ngOnInit = function () {
    };
    PageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page',
            template: __webpack_require__(/*! ./page.component.html */ "./src/app/views/page/page.component.html"),
            styles: [__webpack_require__(/*! ./page.component.scss */ "./src/app/views/page/page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageComponent);
    return PageComponent;
}());



/***/ }),

/***/ "./src/app/views/register/register.component.html":
/*!********************************************************!*\
  !*** ./src/app/views/register/register.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-body\">\r\n  <main class=\"main d-flex align-items-center\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form>\r\n                <h1>Register</h1>\r\n                <p class=\"text-muted\">Create your account</p>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Username\" autocomplete=\"username\" required>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">@</span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" placeholder=\"Email\" autocomplete=\"email\" required>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" placeholder=\"Password\" autocomplete=\"new-password\" required>\r\n                </div>\r\n                <div class=\"input-group mb-4\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" placeholder=\"Repeat password\" autocomplete=\"new-password\" required>\r\n                </div>\r\n                <button type=\"button\" class=\"btn btn-block btn-success\">Create Account</button>\r\n              </form>\r\n            </div>\r\n            <div class=\"card-footer p-4\">\r\n              <div class=\"row\">\r\n                <div class=\"col-6\">\r\n                  <button class=\"btn btn-block btn-facebook\" type=\"button\"><span>facebook</span></button>\r\n                </div>\r\n                <div class=\"col-6\">\r\n                  <button class=\"btn btn-block btn-twitter\" type=\"button\"><span>twitter</span></button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/views/register/register.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/register/register.component.ts ***!
  \******************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RegisterComponent = /** @class */ (function () {
    function RegisterComponent() {
    }
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/views/register/register.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyDwzuQ7jO67cR37sNV2d76-1AXrS6GvLn0",
        authDomain: "app-titivan-1562162494664.firebaseapp.com",
        databaseURL: "https://app-titivan-1562162494664.firebaseio.com",
        projectId: "app-titivan-1562162494664",
        storageBucket: "app-titivan-1562162494664.appspot.com",
        messagingSenderId: "890892421485",
        appId: "1:890892421485:web:287440599b5de522"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"], {
    preserveWhitespaces: true
})
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Usuario\Desktop\Proyectos-Team-SJC\Titivan\admin-titivan-front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map