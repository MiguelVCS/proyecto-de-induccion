import { Component, OnDestroy, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItemsAdmin, navItemsSoporte } from '../../_nav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;

  public perfil = JSON.parse(localStorage.getItem('usuario'));

  constructor(
    private _router: Router,
    @Inject(DOCUMENT) _document?: any
    ) {
      this.changes = new MutationObserver((mutations) => {
        this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
      });
      this.element = _document.body;
      this.changes.observe(<Element>this.element, {
        attributes: true,
        attributeFilter: ['class']
      });

  }

  ngOnInit(){
    
    

    if(this.perfil.type_user == 1){
      this.navItems=navItemsAdmin;
    }
    else{
      if(this.perfil.type_user === 2){
        this.navItems=navItemsSoporte;
      }
    }
    console.log(this.perfil.type_user);
    

/*
    if(this.perfil[0].id_perfil_admin===1){
      this.navItems=navItemsAdmin;
    }
*/
  }
  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  cerrarSession(){
    localStorage.clear();
    this._router.navigate(['/login']);
  }
}
