import { notEqual } from 'assert';

interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItemsAdmin: NavData[] = [
  {
    title: true,
    name: 'Administrador'
  },
  {
    name: 'Inicio',
    url: '/home',
    icon: 'icon-home',
  },
  {
    name: 'Registrar Afiliado',
    url: '/home/administrador/registrarUsuario',
    icon: 'icon-user'
    
  },
  {
    name: 'Eliminar Afiliado',
    url:'/home/administrador/eliminarUsuario',
    icon: 'icon-ban'
    
  },
  {
    name: 'Listar Afiliados',
    url: '/home/administrador/listarUsuario',
    icon: 'icon-people'
  },
  {
    name: 'Editar Afiliado',
    url: '/home/administrador/editarUsuario',
    icon: 'icon-pencil'
  }
];


export const navItemsSoporte: NavData[] = [
  {
    title: true,
    name: 'Soporte'
  },
  {
    name: 'Inicio',
    url: '/home',
  },

];
