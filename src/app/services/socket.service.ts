import { Injectable } from '@angular/core';
import {GLOBAL} from './global';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url:string;
  public socket;
  constructor() { 

    this.url = GLOBAL.url_socket;
    this.socket = io.connect(this.url, {secure: true});
  }

  setRealTimeEmitirRealTime(obj_afiliado){
    this.socket.emit('emitir_real_time', obj_afiliado);
  }

  getRealTimeEmitirRealTime(): Observable<any>{
    return new Observable(observer => {
      this.socket.on('recibir_real_time',(data)=> {
        observer.next(data);
      });
    });
  }



}
