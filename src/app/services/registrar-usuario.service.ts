import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {GLOBAL} from './global';


@Injectable({
  providedIn: 'root'
})
export class RegistrarUsuarioService {

  private url:string;

  constructor(private _http:Http) { 

    this.url = GLOBAL.url;
  }

  registrarUsuario(dataUsuario){

    const headers = new Headers({

      'Content-Type': 'application/json'
        

    });

    return this._http.post(this.url + '/user/insert',dataUsuario,{ headers: headers }).map(res => res.json());


  }
}
