import { TestBed } from '@angular/core/testing';

import { DistritsService } from './distrits.service';

describe('DistritsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DistritsService = TestBed.get(DistritsService);
    expect(service).toBeTruthy();
  });
});
