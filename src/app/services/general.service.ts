import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

    public url: String = '';
    public identity;
    public token='';
    constructor(
      private _http: Http
      ) {
      this.url = GLOBAL.url;
      this.token='Bearer '+localStorage.getItem('token');
    }
}