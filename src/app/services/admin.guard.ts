import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UsuarioService } from './usuario.service';

@Injectable({
    providedIn: 'root'
})
export class AdminGuard implements CanActivate{
    constructor(
        private _router: Router,
        private _usuarioService: UsuarioService
    ){}

    canActivate(){
        let identity = this._usuarioService.getIdentity();
        console.log(identity)

        console.log(identity.type_user);

        if (identity.type_user == 1) {

            console.log('login true');
            return true;
            
        }else{

            this._router.navigate(['/login']);
        }
        /*if(identity.type_user === 1  || identity.type_user === 2){
            console.log('login true');
            return true;
        }else{
            this._router.navigate(['/login']);
            return false;
        }*/
    }
}