import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class EditarUsuarioService {

  private url:string;

  constructor(private _http:Http) {

    this.url = GLOBAL.url;
  }

  editarUsuario(data){

    const headers = new Headers({

      'Content-Type': 'application/json'
      
    });

    return this._http.put(this.url+ '/user/edit',data,{ headers: headers }).map(res => res.json());


  }
}
