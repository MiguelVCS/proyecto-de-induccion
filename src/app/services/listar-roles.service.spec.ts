import { TestBed } from '@angular/core/testing';

import { ListarRolesService } from './listar-roles.service';

describe('ListarRolesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListarRolesService = TestBed.get(ListarRolesService);
    expect(service).toBeTruthy();
  });
});
