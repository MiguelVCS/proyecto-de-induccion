import { Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService{

  public url: String = '';
  public identity;
  //public token=localStorage.getItem('token');
  public token='';
    constructor(
      private _http: Http
      ) { 
      this.url = GLOBAL.url;
      this.token=localStorage.getItem('token');
      console.log("-- token constructor --");
      console.log(this.token);
  }

  login(user_to_login) { 
    const headers = new Headers({ 
      'Content-Type': 'application/json',
      
    });
    return this._http.post(this.url + 'administrador/login', user_to_login , { headers: headers }).map(res => res.json());
  }

  getIdentity() {
    const _identity = JSON.parse(localStorage.getItem('usuario'));
    if (_identity !== 'undefined') {
      console.log("Se seteo el usuario JSON");
      this.identity = _identity;
    } else {
      console.log("Identity setado a null");
      this.identity = null;
    }
    return this.identity;
  }

  registrarUser(user){
    const headers = new Headers({ 
      'Content-Type': 'application/json',
      'Authorization': this.token,
    });
    return this._http.post(this.url+'administrador/registrar',user,{headers:headers}).map(res=>res.json());
  }

  listarAdmins(id){
    const headers = new Headers({ 
      'Content-Type': 'application/json',
      'Authorization': this.token,
    });
    console.log(this.url);
    return this._http.get(this.url+'administrador/list/'+id,{headers:headers}).map(res=>res.json());
  }

  listarDatosPropios(){
    this.token='Bearer '+localStorage.getItem('token');
    const headers = new Headers({ 
      'Content-Type': 'application/json',
      'Authorization': this.token,
    });
    console.log(this.url);
    return this._http.get(this.url+'administrador/list',{headers:headers}).map(res=>res.json());
  }

  listarTiposAdmin(){
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.token
    });
    return this._http.get(this.url + 'administrador/tipos', {headers:headers}).map(res=>res.json());
  }

  cambiarEstado(user){
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.token
    });
    return this._http.post(this.url+'administrador/cambiar-estado',user,{headers:headers}).map(res=>res.json());
  }

  editarDatosPropios(user){
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.token
    });
    return this._http.post(this.url+'administrador/editar',user,{headers:headers}).map(res=>res.json());
  }

  cambiarPassword(user){
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.token
    });
    return this._http.post(this.url+'administrador/cambiar-password',user,{headers:headers}).map(res=>res.json());
  }

  recuperarPassword(correo){
    const data = {
      "correo" : correo
    }
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    return this._http.post(this.url+'administrador/reestablecer-password',data,{headers:headers}).map(res=>res.json());
  }

  enviarCodigo(data){
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    return this._http.post(this.url+'administrador/enviar-codigo',data,{headers:headers}).map(res=>res.json());
  }
}
