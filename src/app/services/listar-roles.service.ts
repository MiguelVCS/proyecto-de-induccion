import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class ListarRolesService {

  private url:string;

  constructor(private _http:Http) {

    this.url = GLOBAL.url;
  }

  listarRoles(){

    const headers = new Headers({

      'Content-Type': 'application/json'
    });

    return this._http.get(this.url+'/role/list',{ headers: headers }).map(res => res.json());
  }
}
