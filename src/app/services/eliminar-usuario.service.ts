import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class EliminarUsuarioService {

  private url:string;
  public token = '';

  constructor(private _http:Http) { 

    this.url = GLOBAL.url;
    this.token = localStorage.getItem('token');
  }

  eliminarUsuario(data){

    const headers = new Headers({

      'Content-Type': 'application/json',
      'Authorization': this.token

    });

    return this._http.put(this.url+'/request-affiliate/update',data,{ headers: headers }).map(res => res.json());


  }
}
