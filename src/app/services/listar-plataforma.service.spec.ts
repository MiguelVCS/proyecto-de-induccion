import { TestBed } from '@angular/core/testing';

import { ListarPlataformaService } from './listar-plataforma.service';

describe('ListarPlataformaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListarPlataformaService = TestBed.get(ListarPlataformaService);
    expect(service).toBeTruthy();
  });
});
