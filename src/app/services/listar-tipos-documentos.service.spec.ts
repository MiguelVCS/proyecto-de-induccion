import { TestBed } from '@angular/core/testing';

import { ListarTiposDocumentosService } from './listar-tipos-documentos.service';

describe('ListarTiposDocumentosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListarTiposDocumentosService = TestBed.get(ListarTiposDocumentosService);
    expect(service).toBeTruthy();
  });
});
