import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {GLOBAL} from './global';

  

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url: string;
  constructor(private _http: Http) { 

    this.url = GLOBAL.url;
  }

  login(data){

    const headers = new Headers({

      'Content-Type': 'application/json'
        

    });

    return this._http.post(this.url + '/user/login',data,{ headers: headers }).map(res => res.json());

  }

}


