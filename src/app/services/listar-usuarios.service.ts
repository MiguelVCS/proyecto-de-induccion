import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class ListarUsuariosService {

  private url:string;
  public token = '';

  constructor(private _http:Http) { 

    this.url = GLOBAL.url;
    this.token=localStorage.getItem('token');

  }

  listarUsuarios(){

    const headers = new Headers({ 
      'Authorization': this.token
    });

    return this._http.get(this.url + '/request-affiliate/list',{headers:headers}).map(res=>res.json());  

  }
}
