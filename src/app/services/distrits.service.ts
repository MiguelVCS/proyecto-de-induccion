import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DistritsService {

  constructor(private _http:Http) { 


  }

  listarDistritos(){

    const headers = new Headers({

      'Content-Type': 'application/json'
    });

    return this._http.get('assets/distrits.json',{ headers: headers }).map(res => res.json())
  }
}
