import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from 'angular-6-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from "ngx-bootstrap";
import { NgxLoadingModule } from 'ngx-loading';
import { ListarUsuariosComponent} from './usuarios/listarUsuario/listar-usuarios.component';

import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CKEditorModule } from 'ng2-ckeditor';
import { AgmCoreModule } from '@agm/core';
import { RegistrarUsuarioComponent } from './usuarios/registrar-usuario/registrar-usuario.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import { EliminarUsuariosComponent } from './usuarios/eliminar-usuarios/eliminar-usuarios.component';
import { EditarUsuarioVistaComponent } from './usuarios/editar-usuario-vista/editar-usuario-vista.component';
@NgModule({
  declarations: [
    ListarUsuariosComponent,
    RegistrarUsuarioComponent,
    EditarUsuarioComponent,
    EliminarUsuariosComponent,
    EditarUsuarioVistaComponent
    
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    TypeaheadModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    NgbModule,
    ModalModule.forRoot(),
    DataTableModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC_X7v9rppknNZXSCkgqR7As_5oxF9_w3s'
    })
  ]
})

export class AdminModule {  }
/*
-10.7499947,-77.768864/ Barranca 
-10.717660, -77.765549 / por ahi
-10.994189,-77.6205434 / Calle, 15160
-12.1493682,-76.9888404 / bolichera


-12.0262674,-77.1282076 / lima
-11.9324474,-77.0662678 / comas
-11.4943909,-77.2112919 / huaral
*/