import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ListarUsuariosComponent} from './usuarios/listarUsuario/listar-usuarios.component';
import {RegistrarUsuarioComponent} from './usuarios/registrar-usuario/registrar-usuario.component';
import { EliminarUsuariosComponent } from './usuarios/eliminar-usuarios/eliminar-usuarios.component';
import { EditarUsuarioComponent } from './usuarios/editar-usuario/editar-usuario.component';
import {EditarUsuarioVistaComponent} from './usuarios/editar-usuario-vista/editar-usuario-vista.component';






const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Administrador'
    },
    children: [

      {
        path: '',
        redirectTo: 'listarUsuario',
        pathMatch: 'full'
        
      },
      {
        path: 'listarUsuario',
        component: ListarUsuariosComponent
      },
      {
        path: 'registrarUsuario',
        component: RegistrarUsuarioComponent
      },
      {
        path: 'eliminarUsuario',
        component: EliminarUsuariosComponent
      },
      {
        path: 'editarUsuario',
        component: EditarUsuarioComponent
      },
      {
        path: 'editarUsuarioVista/:id',
        component:EditarUsuarioVistaComponent
      }
      
      
      

      //registrar-ruta-conductor
    ]
  }
] 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AdminRoutingModule { }
