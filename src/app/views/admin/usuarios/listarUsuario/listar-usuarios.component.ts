import { Component, OnInit } from '@angular/core';
import {ListarUsuariosService} from '../../../../services/listar-usuarios.service';
import {SocketService} from '../../../../services/socket.service';

@Component({
  selector: 'app-listar-usuarios',
  templateUrl: './listar-usuarios.component.html',
  styleUrls: ['./listar-usuarios.component.scss']
})
export class ListarUsuariosComponent implements OnInit {

  public usuarios;
  public listaUsuariosValidos = [];
  

  constructor(private _listarUsuario: ListarUsuariosService,
              private _socket:SocketService) { }

  ngOnInit() {

    this.listarUsuarios();
    this._socket.getRealTimeEmitirRealTime().subscribe(
      data=>{
        this.listarUsuarios();
        console.log(data);
      }

    )
    
    
  }

  listarUsuarios(){
    this.listaUsuariosValidos = [];
    this._listarUsuario.listarUsuarios().subscribe(

      data =>{
        
        this.usuarios = data.requests_affiliates;
        console.log(typeof this.usuarios);
        console.log(this.usuarios);
        this.listarUsuariosValidados();
        


      },
      error => {
        console.log("error " + error);
      }



    );
  }

  listarUsuariosValidados(){
    this.listaUsuariosValidos = [];
    for (let index = 0; index < this.usuarios.length; index++) {
      
      if (this.usuarios[index].flag_role_validate == "1" || this.usuarios[index].flag_role_validate == "0") {

        console.log("Esta iterando");
        
        
        this.listaUsuariosValidos.push(this.usuarios[index]);
      }
      
    }
  }

}
