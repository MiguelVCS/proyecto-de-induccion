import { Component, OnInit } from '@angular/core';
import {ListarUsuariosService} from '../../../../services/listar-usuarios.service';
import {EliminarUsuarioService} from '../../../../services/eliminar-usuario.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.scss']
})
export class EditarUsuarioComponent implements OnInit {

  public listaUsuariosValidos = [];
  public listaUsuarios;

  constructor(private _listarUsuario:ListarUsuariosService,
    private _eliminarUsuario:EliminarUsuarioService,private _router:Router) { }

  ngOnInit() {

    this.listarUsuarios();
  }

  irListarUsuariosVista(idUsuario){

    const listarUsuarioVista = '/home/administrador/editarUsuarioVista';

    this._router.navigate([listarUsuarioVista,idUsuario]);
  }

  listarUsuarios(){

    this._listarUsuario.listarUsuarios().subscribe(

      data =>{

        this.listaUsuarios = data.requests_affiliates;
        console.log("Listo correctamente");

        this.listarUsuariosValidados(this.listaUsuarios);


      },
      error => {
        console.log("error " + error);
      }



    );

    
  }

  listarUsuariosValidados(listaUsuarios){

    for (let index = 0; index < this.listaUsuarios.length; index++) {
      
      if (this.listaUsuarios[index].flag_role_validate == "1" || this.listaUsuarios[index].flag_role_validate == "0") {

        console.log("Esta iterando");
        
        
        this.listaUsuariosValidos.push(this.listaUsuarios[index]);
      }
      
    }
  }

}
