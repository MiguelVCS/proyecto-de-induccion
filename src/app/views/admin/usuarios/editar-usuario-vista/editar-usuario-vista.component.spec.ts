import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarUsuarioVistaComponent } from './editar-usuario-vista.component';

describe('EditarUsuarioVistaComponent', () => {
  let component: EditarUsuarioVistaComponent;
  let fixture: ComponentFixture<EditarUsuarioVistaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarUsuarioVistaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarUsuarioVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
