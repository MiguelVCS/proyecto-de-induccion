import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ListarUsuariosService} from '../../../../services/listar-usuarios.service';
import {Afiliado} from '../afiliado';
import { Usuario } from '../../../../models/usuario';
import {EditarUsuarioService} from '../../../../services/editar-usuario.service';
import {ListarTiposDocumentosService} from '../../../../services/listar-tipos-documentos.service';
import {ListarRolesService} from '../../../../services/listar-roles.service';
import {ListarPlataformaService} from '../../../../services/listar-plataforma.service';
import { parseDate } from 'ngx-bootstrap';
import {SocketService} from '../../../../services/socket.service';


@Component({
  selector: 'app-editar-usuario-vista',
  templateUrl: './editar-usuario-vista.component.html',
  styleUrls: ['./editar-usuario-vista.component.scss']
})
export class EditarUsuarioVistaComponent implements OnInit {
  
  public listaUsuariosValidos = [];
  public defaultDate = new Date();
  public listaUsuarios;
  public listaRoles= [];
  public listaPlataformas = [];
  public tiposDeDocumentos=[];
  public mensajeExitoso="";
  public usuario= {

    "id_user":0,
    "names":"",
    "last_names":"",
    "birthday":"",
    "gender":"",
    "id_type_document":"",
    "value_document":"",
    "email":"",
    "phone":"",
    "id_distrit":"",
    "address":"",
    "id_platform":"",
    "id_role":""


  }
  

  

  constructor(
    private router:Router,
    private route: ActivatedRoute,
    private _listarUsuario:ListarUsuariosService,
    private _router:Router,
    private _editarUsuarioService: EditarUsuarioService,
    private _listarTiposDocumentos:ListarTiposDocumentosService,
    private _listarRoles:ListarRolesService,
    private _listarPlataformas:ListarPlataformaService,
    private _socket:SocketService

  ) { 

    /*this.usuarioAEnviar.id_user = this.usuario.id_user;
    this.usuarioAEnviar.names = this.usuario.names;
    this.usuarioAEnviar.last_names = this.usuario.last_names;
    this.usuarioAEnviar.birthday = this.usuario.birthday;
    this.usuarioAEnviar.gender = this.usuario.gender;
    this.usuarioAEnviar.id_type_document = this.usuario.id_type_document;
    this.usuarioAEnviar.value_document = this.usuario.value_document;
    this.usuarioAEnviar.email = this.usuario.email;
    this.usuarioAEnviar.phone = this.usuario.phone;
    this.usuarioAEnviar.id_distrit = this.usuario.id_distrit;
    this.usuarioAEnviar.address = this.usuario.address;
    this.usuarioAEnviar.id_platform = this.usuario.id_platform;
    this.usuarioAEnviar.id_role = this.usuario.id_role;*/


  }

  ngOnInit() {

    this.listarUsuarios();

    

    /*this.usuarioAEnviar = {

      "id_user":this.usuarioAEnviar.id_user,
      "names":this.usuarioAEnviar.names,
      "last_names":this.usuarioAEnviar.last_names,
      "birthday":this.usuarioAEnviar.birthday,
      "gender":this.usuarioAEnviar.gender,
      "id_type_document":this.usuarioAEnviar.id_type_document,
      "value_document":this.usuarioAEnviar.value_document,
      "email":this.usuarioAEnviar.email,
      "phone":this.usuarioAEnviar.phone,
      "id_distrit":this.usuarioAEnviar.id_distrit,
      "address":this.usuarioAEnviar.address,
      "id_platform":this.usuarioAEnviar.id_platform,
      "id_role":this.usuarioAEnviar.id_role


    }*/


    

    
  }

  listarTiposDocumentos(){

    this._listarTiposDocumentos.listarTiposDocumentos().subscribe(

      data =>{

        this.tiposDeDocumentos = data.types_documents;
        console.log(this.tiposDeDocumentos);

      }
    );
  }

  listarPlataformas(){

    this._listarPlataformas.listarPlataforma().subscribe(

      data=>{

        this.listaPlataformas = data.platforms;
      }

    )
  }

  listarRoles(){

    this._listarRoles.listarRoles().subscribe(

      data=>{

        this.listaRoles = data.roles;
        console.log(this.listaRoles);

      }
    );
  }

  editarUsuario(){

    
    console.log(this.usuario);
    

    this._editarUsuarioService.editarUsuario(this.usuario).subscribe(

      data =>{
        this._socket.setRealTimeEmitirRealTime(this.usuario);
        this.mensajeExitoso = data.message;
        this._router.navigate(['/home/administrador/listarUsuario']);

      },
      error=>{
          
          console.log('estrc',error._body);
          console.log((error));
      }


    );
  }

  listarUsuarios(){

    this._listarUsuario.listarUsuarios().subscribe(

      data =>{

        this.listaUsuarios = data.requests_affiliates;
        console.log("Listo correctamente");

        this.listarUsuariosValidados();

        this.route.paramMap.subscribe(

          parms =>{
    
            if(parms.has("id")){
    
              this.obtenerUsuarioPorId(parms.get("id"));
              
            }
          }
          
        );

        this.listarTiposDocumentos();
        this.listarRoles();
        this.listarPlataformas();


      },
      error => {
        console.log("error " + error);
      }



    );

    

    
  }

  listarUsuariosValidados(){

    for (let index = 0; index < this.listaUsuarios.length; index++) {
      
      if (this.listaUsuarios[index].flag_role_validate == "1" || this.listaUsuarios[index].flag_role_validate == "0") {

        console.log("Esta iterando");
        
        
        this.listaUsuariosValidos.push(this.listaUsuarios[index]);
        
      }
      
    }
  }

  obtenerUsuarioPorId(idUsuario){

    //let idUsuarioNumber = parseInt(idUsuario);


    /*console.log("Se llamo al metodo obtenerUsuarioPorId");
    console.log(this.listaUsuariosValidos);*/

    for (let index = 0; index < this.listaUsuariosValidos.length; index++) {
      
      if(this.listaUsuariosValidos[index].id_user == idUsuario){
        console.log(this.listaUsuariosValidos[index]);

        this.usuario.id_user = this.listaUsuariosValidos[index].id_user;
        this.usuario.names = this.listaUsuariosValidos[index].names;
        this.usuario.last_names = this.listaUsuariosValidos[index].last_names;
        this.usuario.birthday = this.listaUsuariosValidos[index].birthday;
        this.usuario.gender = this.listaUsuariosValidos[index].gender;
        this.usuario.id_type_document = this.listaUsuariosValidos[index].id_type_document;
        this.usuario.value_document = this.listaUsuariosValidos[index].value_document;
        this.usuario.email = this.listaUsuariosValidos[index].email;
        this.usuario.phone = this.listaUsuariosValidos[index].phone;
        this.usuario.id_distrit = this.listaUsuariosValidos[index].id_distrit;
        this.usuario.address = this.listaUsuariosValidos[index].address;
        this.usuario.id_platform = this.listaUsuariosValidos[index].id_platform;
        this.usuario.id_role = this.listaUsuariosValidos[index].id_role;
        

        
      
        console.log('Este es el usuario enviado desde editarUsuario',this.usuario);
        

      }
      
    }

    /*console.log("A continuacion el valor de un objeto del arreglo listaUsuariosValidos")

    console.log(this.listaUsuariosValidos[0]);*/


  }

  


}
