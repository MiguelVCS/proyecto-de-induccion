import { Component, OnInit } from '@angular/core';
import {RegistrarUsuarioService} from '../../../../services/registrar-usuario.service';
import {ListarTiposDocumentosService} from '../../../../services/listar-tipos-documentos.service';
import {ListarRolesService} from '../../../../services/listar-roles.service';
import {ListarPlataformaService} from '../../../../services/listar-plataforma.service';
import {DistritsService} from '../../../../services/distrits.service';
import {Router} from '@angular/router';
import {SocketService} from '../../../../services/socket.service';

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.component.html',
  styleUrls: ['./registrar-usuario.component.scss']
})
export class RegistrarUsuarioComponent implements OnInit {

  public listaRoles= [];
  public listaPlataformas = [];
  public tiposDeDocumentos=[];
  public listaDistritos=[];

  public usuario = {

    "code_user":"T" + this.generarNumeroAleatorio() + "UX" + this.generarNumeroAleatorio(),
    "code_sponsor":"DCGU00001",
    "names":"",
    "last_names":"",
    "birthday":"",
    "gender":"",
    "id_type_document":"",
    "value_document":"",
    "email":"",
    "phone":"",
    "id_distrit":"",
    "address":"",
    "id_platform":"",
    "id_role":"",
    "password_user":""


  };

  public admin = {
    "email":"",
    "password":""

  };

  public mensajeYaRegistrado = "";
  public mensajeRegistradoExitosamente = "";

  public validateEmail=false;

  constructor(private _registrarUsuarioService:RegistrarUsuarioService,
              private _listarTiposDocumentos:ListarTiposDocumentosService,
              private _listarRoles:ListarRolesService,
              private _listarPlataformas:ListarPlataformaService,
              private _listarDistrito:DistritsService,
              private _router:Router,
              private _socketService:SocketService) { }

  ngOnInit() {

    this.listarTiposDocumentos();

  }

  listarTiposDocumentos(){

    this._listarTiposDocumentos.listarTiposDocumentos().subscribe(

      data =>{

        this.tiposDeDocumentos = data.types_documents;
        this.listarPlataformas();
        this.listarRoles();
        this.listarDistritos();
        console.log(this.tiposDeDocumentos);

      }
    );
  }

  listarPlataformas(){

    this._listarPlataformas.listarPlataforma().subscribe(

      data=>{

        this.listaPlataformas = data.platforms;
      }

    )
  }

  listarRoles(){

    this._listarRoles.listarRoles().subscribe(

      data=>{

        this.listaRoles = data.roles;
        console.log(this.listaRoles);

      }
    );
  }

  listarDistritos(){

    this._listarDistrito.listarDistritos().subscribe(

      data=>{

        this.listaDistritos = data.distrits;
        console.log(this.listaDistritos);
      }
    )


  }

  registrarUsuario(usuario){

    var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(regex.test(this.usuario.email) == false){
      
      this.validateEmail = true;
    }else{
      this.validateEmail=false;
    }

    if (this.validateEmail == false && this.validarInputs() == true) {

      this._registrarUsuarioService.registrarUsuario(usuario).subscribe(

        data=>{

          if (data.code == 200) {
            this._socketService.setRealTimeEmitirRealTime(this.usuario);
            this.mensajeRegistradoExitosamente = "Se registro exitosamente";
            this._router.navigate(['/home/administrador/listarUsuario']);
          }

          if (data.code == 400) {
            
            this.mensajeYaRegistrado = "Ya se registro anteriormente";
          }
        },
        error=>{
          
          console.log('estrc',error._body);
          console.log((error));
        }
      )
    }
  }

  validarInputs(){

    let inputsCorrectos = true;

    if(this.usuario.names == "" || this.usuario.last_names == "" || 
      this.usuario.birthday == "" || this.usuario.gender == "" ||
      this.usuario.id_type_document == "" || this.usuario.value_document == "" ||
      this.usuario.email == "" || this.usuario.phone == "" ||
      this.usuario.id_distrit == "" || this.usuario.address == "" ||
      this.usuario.id_platform == "" || this.usuario.id_role == "" || this.usuario.password_user == ""){

      inputsCorrectos = false;

    }

    return inputsCorrectos;


  }

  generarNumeroAleatorio(){

    let numeroAleatorio= Math.floor(Math.random()*10) + 1;
    return numeroAleatorio;
  }

}
