import { Component, OnInit } from '@angular/core';
import {ListarUsuariosService} from '../../../../services/listar-usuarios.service';
import {EliminarUsuarioService} from '../../../../services/eliminar-usuario.service';
import {Router} from '@angular/router';
import {Afiliado} from '../afiliado';
import {SocketService} from '../../../../services/socket.service';

@Component({
  selector: 'app-eliminar-usuarios',
  templateUrl: './eliminar-usuarios.component.html',
  styleUrls: ['./eliminar-usuarios.component.scss']
})
export class EliminarUsuariosComponent implements OnInit {

  public listaUsuariosValidos = [];
  public listaUsuarios;
  public usuario:Afiliado;
  public usuarioEliminar = {

    "id_user":0,
    "id_role":0,
    "email_user":"",
    "flag_role_validate":"2"



    

  };

  constructor(private _listarUsuario:ListarUsuariosService,
              private _eliminarUsuario:EliminarUsuarioService,
              private _router:Router,
              private _socket:SocketService) { }

  ngOnInit() {

    this.listarUsuarios();
    
  }

  listarUsuarios(){

    this._listarUsuario.listarUsuarios().subscribe(

      data =>{

        this.listaUsuarios = data.requests_affiliates;
        console.log("Listo correctamente");

        this.listarUsuariosValidados(this.listaUsuarios);


      },
      error => {
        console.log("error " + error);
      }



    );

    
  }

  listarUsuariosValidados(listaUsuarios){

    this.listaUsuariosValidos = [];

    for (let index = 0; index < this.listaUsuarios.length; index++) {
      
      if (this.listaUsuarios[index].flag_role_validate == "1" || this.listaUsuarios[index].flag_role_validate == "0") {

        console.log("Esta iterando");
        
        
        this.listaUsuariosValidos.push(this.listaUsuarios[index]);
        console.log("Lista de usuarios validos");
        console.log(this.listaUsuariosValidos);
      }
      
    }

    console.log(this.listaUsuariosValidos);
  }

  eliminarUsuario(idUsuario){

    let usuarioAEliminar = this.listaUsuariosValidos[idUsuario];

    this.usuarioEliminar.id_user = usuarioAEliminar.id_user;
    this.usuarioEliminar.id_role = usuarioAEliminar.id_role;
    this.usuarioEliminar.email_user = usuarioAEliminar.email;
    const respuestaUsuario = confirm("Estas seguro que quieres eliminar");
    
    if (respuestaUsuario) {
      console.log("Entro a Eliminar");
      this._eliminarUsuario.eliminarUsuario(this.usuarioEliminar).subscribe(

        data=>{

          this._socket.setRealTimeEmitirRealTime(this.usuarioEliminar);
          console.log("Se ejecuto la actualizacion");
          //this.listarUsuarios(); Si quieres refrescar la pagina de manera reactiva mediante el concepto de propagacion del cambio
          this._router.navigate(['/home/administrador/listarUsuario']);

          


        },
        error => {
        console.log("error " + error);
        }
      );
    }
  }

  obtenerUsuarioPorId(idUsuario):Afiliado{

    let idUsuarioNumber = parseInt(idUsuario);


    /*console.log("Se llamo al metodo obtenerUsuarioPorId");
    console.log(this.listaUsuariosValidos);*/

    for (let index = 0; index < this.listaUsuariosValidos.length; index++) {
      
      if(this.listaUsuariosValidos[index].id_user == idUsuarioNumber){
        
        this.usuario = this.listaUsuariosValidos[index];
        console.log(this.usuario);

      }
      
    }

    /*console.log("A continuacion el valor de un objeto del arreglo listaUsuariosValidos")

    console.log(this.listaUsuariosValidos[0]);*/
    return this.usuario;

  }



  

  



}
