import { Component, ViewChild, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../models/usuario';
import { ModalDirective } from 'ngx-bootstrap';
import {LoginService} from '../../services/login.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [UsuarioService, NgbModalConfig, NgbModal,LoginService],
})
export class LoginComponent{

  @ViewChild('dangerModal') public dangerModal: ModalDirective;


  public user: Usuario;
  public message: string;
  public loading = false;
  public customLoadingTemplate;
  public showAlertLogin;
  public mensaje;
  public logeo = {
    email_admin: "",
    password_admin: ""
  }
  passConfirmar = "";
  reestablecerPassword = {
    correo: "",
    codigo : "",
    password : ""
  }
  
  recuperar = true;


  passwordtype = "password";
  passwordshow = false;
  __icon = "icon-lock";

  passwordtype_new = "password";
  passwordshow_new = false;
  __icon_new = "icon-lock";

  passwordtype_conf = "password";
  passwordshow_conf = false;
  __icon_conf = "icon-lock";

  
  constructor(
    config: NgbModalConfig,
    private modalService: NgbModal,
    private _router: Router,
    private _usuarioService: UsuarioService,
    private _loginService: LoginService
  ) {
    //config.backdrop = 'static';
    //config.keyboard = false;
  }

  ngOnInit() {
    localStorage.clear();
    this.showAlertLogin = false;
    this.message = "";
  }

  login() {
    //this._router.navigate(['/home']);

    /*
      this._usuarioService.login(this.logeo).subscribe(
       data=>{
         if(data.code===200){
           console.log(data.token);
           localStorage.setItem('usuario', JSON.stringify(data.usuario));
           localStorage.setItem('token',data.token);
           this._router.navigate(['/home']);
         }
         else{
            this.message = data.message;
            this.showAlertLogin = true;
            this.logeo.password="";
            this.logeo.correo="";
         }
       },
       error=>{
         console.log(error)
       }
     );
     */

     this._loginService.login(this.logeo).subscribe(

        data=>{

          if(data.code === 200){
            localStorage.setItem('email_admin',data.usuario.email_admin);
            localStorage.setItem('usuario',JSON.stringify(data.usuario));
            localStorage.setItem('token',data.token);
            this._router.navigate(['/home']); 
          
          }
        },
        error=>{
          this.mensaje = JSON.parse(error._body).message; 
          console.log('estrc',error._body);
          console.log((error));
        }
        

     )
  }

  removeAlertLogin() {
    this.showAlertLogin = false;
  }

  recuperarPassword(){
    if (this.logeo.email_admin.length != 0) {
      this._usuarioService.recuperarPassword(this.logeo.email_admin).subscribe(
        data=>{
          if(data.code === 200){
            this.logeo.password_admin="";
            this.reestablecerPassword.correo = this.logeo.email_admin
            this.recuperar=true;
            this.dangerModal.show();
            this.dangerModal.config.backdrop = 'static';
          }
          else{
            alert(data.message);
          }
        },
        error=>{
          console.log(error);
        }
      );
    }
    else{
      alert("Debe ingresar un correo");
    }
  }

  validarCodigo(){
    if(this.passConfirmar == this.reestablecerPassword.password){
      this._usuarioService.enviarCodigo(this.reestablecerPassword).subscribe(
        data=>{
          alert(data.message);
          if(data.code === 200){
            this.dangerModal.hide();
          }
        },
        error=>{
          console.log(error);
        }
      )
    }
    else{
      alert("Las contraseñas no coinciden")
    }
    this.limpiar();
  }
  
  togglePass(num) {
    console.log("ver / ocultar");
    switch (num) {
      case 1: if (this.passwordshow) {
        this.passwordtype = "password";
        this.passwordshow = false;
        this.__icon = "icon-lock";
      } else {
        this.passwordtype = "text";
        this.passwordshow = true;
        this.__icon = "icon-lock-open";
      }; break;
      case 2: if (this.passwordshow_new) {
        this.passwordtype_new = "password";
        this.passwordshow_new = false;
        this.__icon_new = "icon-lock";
      } else {
        this.passwordtype_new = "text";
        this.passwordshow_new = true;
        this.__icon_new = "icon-lock-open";
      }break;
      case 3: if (this.passwordshow_conf) {
        this.passwordtype_conf = "password";
        this.passwordshow_conf = false;
        this.__icon_conf = "icon-lock";
      } else {
        this.passwordtype_conf = "text";
        this.passwordshow_conf= true;
        this.__icon_conf = "icon-lock-open";
      } ModalDirective;
    }
  }

  limpiar(){
    this.reestablecerPassword.codigo = "";
    this.reestablecerPassword.password = "";
    this.passConfirmar = "";
  }
}
