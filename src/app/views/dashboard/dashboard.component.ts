import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { GeneralService } from '../../services/general.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(
    private _generalService: GeneralService
  ) {
  }
  radioModel: string;

  // mainChart

  public mainChartElementsA = 14;
  public mainChartDataA1: Array<number> = [];
  public mainChartDataA2: Array<number> = [];
  loading = false;

  public mainChartDataA: Array<any> = [
    {
      data: this.mainChartDataA1,
      label: 'Indicador 1'
    },
    {
      data: this.mainChartDataA2,
      label: 'Indicador 2'
    }
  ];

  public mainChartElementsB = 12;
  public mainChartDataB1: Array<number> = [];
  public mainChartDataB2: Array<number> = [];

  public mainChartDataB: Array<any> = [
    {
      data: this.mainChartDataB1,
      label: 'Indicador 1'
    },
    {
      data: this.mainChartDataB2,
      label: 'Indicador 2'
    }
  ];




  public mainChartElementsC = 5;
  public mainChartDataC1: Array<number> = [];
  public mainChartDataC2: Array<number> = [];

  public mainChartDataC: Array<any> = [
    {
      data: this.mainChartDataC1,
      label: 'Ingresos de Itinerarios'
    },
    {
      data: this.mainChartDataC2,
      label: 'Número de Itinerarios'
    },
  ];


  /* tslint:disable:max-line-length */
  public mainChartLabelsA: Array<any> = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
  public mainChartLabelsB: Array<any> = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  public mainChartLabelsC: Array<any> = [];
  
  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips,
      intersect: true,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        labelColor: function(tooltipItem, chart) {
          return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          callback: function(value: any) {
            return value;
          }
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          //max: 250
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  reporte = {
    usuarios_activos : 0,
    conductores_activos : 0,
    viajes_terminados : 0,
    viajes_en_curso : 0
  }

  ngOnInit(): void {
    // generate random values for mainChart
    
    for (let i = 0; i <= this.mainChartElementsA; i++) {
      this.mainChartDataA1.push(0);
      this.mainChartDataA2.push(0);
    }

    for (let i = 0; i <= this.mainChartElementsB; i++) {
      this.mainChartDataB1.push(0);
      this.mainChartDataB2.push(0);
    }

    for (let i = 0; i <= this.mainChartElementsB; i++) {
      this.mainChartDataC1.push(0);
      this.mainChartDataC2.push(0);
    }


    this.obtenerReporte();
  }

  ngOnDestroy(){
    this.loading = false;
  }

  obtenerReporte(){
    this.loading = true;
    const fechaActual = new Date();
    for(let i=1; i<7-fechaActual.getUTCDay(); i++){
      this.mainChartLabelsA.unshift(this.mainChartLabelsA.pop());
    }
    for(let i=1; i<12-fechaActual.getUTCMonth(); i++){
      this.mainChartLabelsB.unshift(this.mainChartLabelsB.pop());
    }
    for(let i=0, j=fechaActual.getUTCFullYear(); i<5; i++, j--){
      this.mainChartLabelsC.unshift(j);
    }
    this.radioModel = 'Mes';
    this.loading = false;
    /*
    this._generalService.reporteGeneral().subscribe(
      data=>{
        if(data.code === 200){
          this.reporte = data.reporte;
          data.reporte_y.forEach(y => {
            let index = y.an - (fechaActual.getUTCFullYear() - 4);
            if(!y.ingresos) y.ingresos = 0;
            this.mainChartDataC1[index] = y.ingresos;
            this.mainChartDataC2[index] = y.n_itinerarios;
          });
          data.reporte_m.forEach(m =>{
            let index = (m.mes - (fechaActual.getUTCMonth() - 11));
            if(!m.ingresos) m.ingresos = 0;
            this.mainChartDataB1[index - 1] = m.ingresos;
            this.mainChartDataB2[index - 1] = m.n_itinerarios;
          })
          data.reporte_d.forEach(d => {
            let index = 14 - Math.round((fechaActual.getTime() - new Date(d.fecha).getTime()) / (1000*60*60*24));
            console.log(index);
            if(!d.ingresos) d.ingresos = 0;
            this.mainChartDataA1[index] = d.ingresos;
            this.mainChartDataA2[index] = d.n_itinerarios;
          });
          this.radioModel = 'Mes';
          this.loading = false;
        }
      },
      error=>{
        console.log(error);
      }
    */
  }
}
